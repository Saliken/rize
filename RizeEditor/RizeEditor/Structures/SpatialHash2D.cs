﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RizeEditor.Components;
using Microsoft.Xna.Framework;

namespace RizeEditor.Structures
{
    class SpatialHash2D
    {
        private int cellSize;
        private Dictionary<Vector2, List<GameObject>> hash;

        public SpatialHash2D(int cellSize)
        {
            this.cellSize = cellSize;
            hash = new Dictionary<Vector2, List<GameObject>>();
        }

        public void AddObject(GameObject obj)
        {
            if (!hash.ContainsKey(GetHash(obj))) hash[GetHash(obj)] = new List<GameObject>();
            hash[GetHash(obj)].Add(obj);
        }

        public List<GameObject> GetObjectsIn(Vector2 Location)
        {
            //Get Bucket
            return hash[GetHash(Location)];
        }

        public List<GameObject> GetObjectsAround(Vector2 Location, int steps)
        {
            List<GameObject> tempList =  new List<GameObject>();
            Vector2 tHash = GetHash(Location);
            for(int x = -steps; x <= steps; x++)
            {
                for (int y = -steps; y <= steps; y++)
                {
                    if (hash.ContainsKey(new Vector2(tHash.X + x, tHash.Y + y)))
                    {
                        tempList.AddRange(hash[new Vector2(tHash.X + x, tHash.Y + y)]);
                    }
                }
            }

            return tempList;
        }

        public void RemoveObject(GameObject obj)
        {
            if (hash[GetHash(obj)].Contains(obj))
            {
                hash[GetHash(obj)].Remove(obj);
            }
            else
            {
               throw new ArgumentException("Object doesn't exist in bucket, It has been moved and not updated");
            }
        }

        public void RemoveObjectMoved(GameObject obj)
        {
            if (hash.ContainsKey(GetHash(new Vector2(obj.PrevX, obj.PrevY))) && hash[GetHash(new Vector2(obj.PrevX, obj.PrevY))].Contains(obj))
            {
                hash[GetHash(new Vector2(obj.PrevX, obj.PrevY))].Remove(obj);
            }
            else
            {
                throw new ArgumentException("Object doesn't exist in bucket");
            }
        }

        private Vector2 GetHash(GameObject obj)
        {
            Vector2 temp = new Vector2();
            temp.X = (int)(obj.X / cellSize);
            temp.Y = (int)(obj.Y / cellSize);
            return temp;            
        }

        private Vector2 GetHash(Vector2 location)
        {
            Vector2 temp = new Vector2();
            temp.X = (int)(location.X / cellSize);
            temp.Y = (int)(location.Y / cellSize);
            return temp;
        }
    }
}
