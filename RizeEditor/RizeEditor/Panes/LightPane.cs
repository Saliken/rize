﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RizeEditor.Components.Lighting;
using RizeEditor.Utilities.GUI;
using RizeEditor.Utilities.UserInput;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RizeEditor.Panes
{
    class LightPane:EditPane
    {
        private CLightSource source;

        private GUI_STextBox txtRange;
        private GUI_STextBox txtColorR;
        private GUI_STextBox txtColorG;
        private GUI_STextBox txtColorB;
        private GUI_STextBox txtIntensity;

        public LightPane(Game game, CLightSource source)
            :base(game)
        {
            this.source = source;

            txtRange = new GUI_STextBox(game, new Rectangle(game.GraphicsDevice.Viewport.TitleSafeArea.Width - 300, game.GraphicsDevice.Viewport.TitleSafeArea.Height - 600, 100, 50), Color.LightGray);
            txtIntensity = new GUI_STextBox(game, new Rectangle(txtRange.Bounds.X, txtRange.Bounds.Bottom + 10, 150, 50), Color.LightGray);
            txtColorR = new GUI_STextBox(game, new Rectangle(txtIntensity.Bounds.X - 100, txtIntensity.Bounds.Bottom + 10, 75, 50), Color.LightGray);
            txtColorG = new GUI_STextBox(game, new Rectangle(txtColorR.Bounds.Right + 15, txtColorR.Bounds.Y, 75, 50), Color.LightGray);
            txtColorB = new GUI_STextBox(game, new Rectangle(txtColorG.Bounds.Right + 15, txtColorG.Bounds.Y, 75, 50), Color.LightGray);

            txtRange.Text = source.Range.ToString();
            txtIntensity.Text = source.Intensity.ToString();
            txtColorR.Text = source.R.ToString();
            txtColorG.Text = source.G.ToString();
            txtColorB.Text = source.B.ToString();
        }

        public override void Update(Microsoft.Xna.Framework.GameTime GT)
        {
            txtRange.Update(GT);
            txtIntensity.Update(GT);
            txtColorR.Update(GT);
            txtColorG.Update(GT);
            txtColorB.Update(GT);

            if (Input.IsPressed(Keys.Enter))
            {
                int tempI;
                float tempF;
                float.TryParse(txtRange.Text, out tempF);
                if (tempF != -1) { source.Range = tempF; }
                float.TryParse(txtIntensity.Text, out tempF);
                if (tempF != -1) { source.Intensity = tempF; }
                int.TryParse(txtColorR.Text, out tempI);
                if (tempI != -1) { source.R = tempI; }
                int.TryParse(txtColorG.Text, out tempI);
                if (tempI != -1) { source.G = tempI; }
                int.TryParse(txtColorB.Text, out tempI);
                if (tempI != -1) { source.B = tempI; }

                txtRange.Text = source.Range.ToString();
                if (source.Intensity == 0.01f) { txtIntensity.Text = ""; }
                else { txtIntensity.Text = source.Intensity.ToString(); }
                txtColorR.Text = source.R.ToString();
                txtColorG.Text = source.G.ToString();
                txtColorB.Text = source.B.ToString();
            }
        }

        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch SB)
        {
            txtRange.Draw(SB);
            txtIntensity.Draw(SB);
            txtColorR.Draw(SB);
            txtColorG.Draw(SB);
            txtColorB.Draw(SB);
        }

        public override void Destroy()
        {
            source = null;

            txtRange.Enabled = false;
            txtRange = null;
            txtIntensity.Enabled = false;
            txtIntensity = null;
            txtColorR.Enabled = false;
            txtColorR = null;
            txtColorG.Enabled = false;
            txtColorG = null;
            txtColorB.Enabled = false;
            txtColorB = null;
            base.Destroy();
        }
    }
}
