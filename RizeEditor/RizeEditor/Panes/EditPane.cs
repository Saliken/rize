﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RizeEditor.Panes
{
    abstract class EditPane
    {
        private Game game;
        public EditPane(Game game)
        {
            this.game = game;
        }

        public abstract void Update(GameTime GT);
        public abstract void Draw(SpriteBatch SB);
        public virtual void Destroy()
        {
            game = null;
        }
    }
}
