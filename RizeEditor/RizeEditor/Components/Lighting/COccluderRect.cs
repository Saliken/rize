﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Krypton;
using Krypton.Common;
using Microsoft.Xna.Framework;
using RizeEditor.Managers;

namespace RizeEditor.Components.Lighting
{
    class COccluderRect:Component
    {
        private ShadowHull hull;

        public COccluderRect(Vector2 dimensions, float XPos, float YPos)
            : base()
        {
            hull = ShadowHull.CreateRectangle(dimensions);
            hull.Position = new Vector2(XPos, YPos);
            RenderManager.LightEngine.Hulls.Add(hull);
        }

        public override void Update(GameTime gameTime)
        {
            hull.Position = Owner.Position;
            hull.Angle = Owner.Rotation;
            base.Update(gameTime);
        }

        public override void OnDestroy()
        {
            RenderManager.LightEngine.Hulls.Remove(hull);
            hull = null;
            base.OnDestroy();
        }
    }
}
