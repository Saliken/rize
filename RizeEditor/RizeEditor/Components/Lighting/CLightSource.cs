﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Krypton.Lights;
using Krypton;
using RizeEditor.Managers;
using RizeEditor.Panes;
using Microsoft.Xna.Framework;

namespace RizeEditor.Components.Lighting
{
    class CLightSource:Component
    {
        private Light2D light;
        public float Range { get { return light.Range; } set { light.Range = value; } }
        public float Intensity { get { return light.Intensity; } set { light.Intensity = value; } }
        public int R { get { return light.Color.R; } set { light.Color = new Color(value, light.Color.G, light.Color.B); } }
        public int G { get { return light.Color.G; } set { light.Color = new Color(light.Color.R, value, light.Color.B); } }
        public int B { get { return light.Color.B; } set { light.Color = new Color(light.Color.R, light.Color.G, value); } }

        public CLightSource(Color color, int range, float XPos, float YPos)
            : base()
        {
            light = new Light2D()
            {
                Texture = LightTextureBuilder.CreatePointLight(RenderManager.graphicsDevice, 128),
                X = XPos,
                Y = YPos,
                Range = range,
                Color = color,
                ShadowType = ShadowType.Solid
            };

            RenderManager.LightEngine.Lights.Add(light);
        }

        public override void OnDestroy()
        {
            RenderManager.LightEngine.Lights.Remove(light);
            light = null;
            base.OnDestroy();
        }

        public override void Update(GameTime gameTime)
        {
            light.Position = Owner.Position;
            base.Update(gameTime);
        }

        public override Tuple<string, EditPane> GetPane()
        {
            return new Tuple<string, EditPane>("Light Source", new LightPane(Global.Game, this));
        }
    }
}
