﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RizeEditor.Components.Tile
{
    class CTileGroup:Component
    {
        private int group;
        public int Group { get { return group; } set { group = (int)MathHelper.Clamp(value, 0, 6); OnGroupChange(); } }

        public CTileGroup()
            :base()
        {
            ShouldUpdate = false;
            group = 0;
        }

        private void OnGroupChange()
        {
            CRenderTile tile = Owner.GetComponent<CRenderTile>();
            if (tile != null)
            {
                switch (group)
                {
                    case 1:
                        tile.Color = Color.Red;
                        break;
                    case 2:
                        tile.Color = Color.Green;
                        break;
                    case 3:
                        tile.Color = Color.CornflowerBlue;
                        break;
                    case 4:
                        tile.Color = Color.Yellow;
                        break;
                    case 5:
                        tile.Color = Color.LightGray;
                        break;
                    case 6:
                        tile.Color = Color.Purple;
                        break;
                }
            }
        }        
    }
}
