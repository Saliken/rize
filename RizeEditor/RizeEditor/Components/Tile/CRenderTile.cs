﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RizeEditor.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RizeEditor.Components.Tile
{
    class CRenderTile:CRender
    {
        public int ID { get; set; }
        public Color Color { get; set; }
        public bool autoOff;

        public CRenderTile(RenderLayers layer) : this(layer, 0, true) { }

        public CRenderTile(RenderLayers layer, int ID, bool aOff)
            : base(layer)
        {
            Color = Color.White;
            this.ID = ID;
            autoOff = aOff;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (ID != 0)
            {
                spriteBatch.Draw(TileManager.TileSheet, new Vector2(Owner.X, Owner.Y), TileManager.GetSourceFromID(ID), Color, 0f, new Vector2(TileManager.tileWidth/2, TileManager.tileHeight/2), 1f, SpriteEffects.None, 1f);
            }
            base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (autoOff) this.ShouldUpdate = false;
        }
    }
}
