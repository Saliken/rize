﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using RizeEditor.Managers;

namespace RizeEditor.Components
{
    class CRenderSprite:CRender
    {
        public Texture2D Texture;
        public Color Color;

        public CRenderSprite(Texture2D texture, RenderLayers layer)
            : base(layer)
        {
            Texture = texture;
            Color = Color.White;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, new Vector2(Owner.X, Owner.Y), null, Color, Owner.Rotation, new Vector2(Texture.Width/2, Texture.Height/2), 1f, SpriteEffects.None, 1f);
        }
    }
}
