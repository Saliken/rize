﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using RizeEditor.Managers;

namespace RizeEditor.Components
{
    class CRender:Component
    {
        public RenderLayers Layer;
        public CRender(RenderLayers layer)
            :base()
        {
            Layer = layer;
        }

        public override void OnInit()
        {
            RenderManager.AddRenderComponent(Layer, this);
            base.OnInit();
            Owner.HasMoved = false;
        }

        public override void OnDestroy()
        {
            RenderManager.RemoveRenderComponent(Layer, this);
            base.OnDestroy();
        }

        public override void Update(GameTime gameTime)
        {
            if (Owner.HasMoved)
            {
                RenderManager.UpdateObject(this.Owner);
            }
            base.Update(gameTime);
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {

        }
    }
}
