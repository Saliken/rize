﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using RizeEditor.Managers;
using FarseerPhysics;

namespace RizeEditor.Components
{
    class CPhysRecSensor:Component
    {
        public bool IsColliding { get; private set; }
        public Body body;
        public CPhysRecSensor(float width, float height, Vector2 position, Category category, Category collidesWith)
            : base()
        {
            body = BodyFactory.CreateRectangle(PhysicsManager.PhysicsWorld, ConvertUnits.ToSimUnits(width), ConvertUnits.ToSimUnits(height), 1f, ConvertUnits.ToSimUnits(position), this);
            body.OnCollision += OnCollision;
            body.CollisionCategories = category;
            body.CollidesWith = collidesWith;
            body.SleepingAllowed = true;
            body.BodyType = BodyType.Dynamic;
            body.IsSensor = true;
            IsColliding = false;
        }

        public void IgnoreWith(Body b)
        {
            body.IgnoreCollisionWith(b);
        }

        public override void OnDestroy()
        {
            PhysicsManager.PhysicsWorld.RemoveBody(body);
            body.OnCollision -= OnCollision;
            body = null;
            base.OnDestroy();
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            SetPosition(Owner.X, Owner.Y);
            SetRotation(Owner.Rotation);
            //if (IsColliding)
            //{
               
                var c = body.ContactList;
                bool temp = false;
                while(c != null)
                {
                    if (c.Contact.IsTouching)
                    {
                        temp = true;
                        break;
                    }
                    c = c.Next;
                }

                IsColliding = temp;
                
            //}
            base.Update(gameTime);
        }

        private bool OnCollision(Fixture f1, Fixture f2, Contact contact)
        {
            IsColliding = true;

            return true;
        }

        private void OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            IsColliding = false;
        }
        public void SetRotation(float rotation)
        {
            body.Rotation = rotation;
        }

        public void SetPosition(float X, float Y)
        {
            //body.SetTransform(new Vector2(ConvertUnits.ToSimUnits(X), ConvertUnits.ToSimUnits(Y)), body.Rotation);
            body.LinearVelocity = Vector2.Zero;
            Vector2 curPos = ConvertUnits.ToDisplayUnits(body.Position);
            body.ApplyLinearImpulse(new Vector2(X - curPos.X, Y - curPos.Y) * body.Mass);
        }
    }
}
