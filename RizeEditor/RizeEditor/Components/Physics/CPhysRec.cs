﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics;
using RizeEditor.Managers;
using Microsoft.Xna.Framework;

namespace RizeEditor.Components
{
    class CPhysRec:Component
    {
        //Rectangular Physics Collision
        private Body body;
        public Body Body { get { return body; } set { body = value; } }
        

        public CPhysRec(float width, float height, float density, Vector2 position, float rotation, Category category, Category collidesWith, BodyType type)
            :base()
        {
            
            body = BodyFactory.CreateRectangle(PhysicsManager.PhysicsWorld, ConvertUnits.ToSimUnits(width), ConvertUnits.ToSimUnits(height), density, ConvertUnits.ToSimUnits(position), this);
            body.Rotation = rotation;
            body.OnCollision += OnCollision;
            body.OnSeparation += OnSeparation;
            body.CollisionCategories = category;
            body.CollidesWith = collidesWith;
            body.SleepingAllowed = true;
            body.BodyType = type;
            body.UserData = this;
        }

        public override void Update(GameTime gameTime)
        {
            if (Body.Enabled)
            {
                Owner.X = ConvertUnits.ToDisplayUnits(body.Position.X);
                Owner.Y = ConvertUnits.ToDisplayUnits(body.Position.Y);
                Owner.Rotation = body.Rotation;
            }
            base.Update(gameTime);
        }

        private bool OnCollision(Fixture f1, Fixture f2, Contact contact)
        {
            List<Component> cs = Owner.GetComponents();
            foreach (Component c in cs)
            {
                c.OnCollisionEnter(f2, contact);
            }

            return true;
        }

        private void OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {

            List<Component> cs = Owner.GetComponents();
            foreach (Component c in cs)
            {
                c.OnCollisionExit(fixtureB);
            }
        }

        public void SetPosition(float X, float Y)
        {
            Body.SetTransform(new Vector2(ConvertUnits.ToSimUnits(X), ConvertUnits.ToSimUnits(Y)), Body.Rotation);
        }

        public void SetRotation(float rot)
        {
            Body.Rotation = rot;
        }
    }
}
