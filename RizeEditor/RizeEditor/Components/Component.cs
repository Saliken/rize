﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using RizeEditor.Panes;

namespace RizeEditor.Components
{
    abstract class Component
    {
        private GameObject owner;
        public GameObject Owner { get { return owner; } set { owner = value; } }
        private bool shouldUpdate;
        public bool ShouldUpdate { get { return shouldUpdate; } set { shouldUpdate = value; } }

        public Component()
        {
            shouldUpdate = true;
        }

        public virtual void OnInit()
        {

        }

        public virtual void OnDestroy()
        {
            owner = null;
        }

        public virtual void OnCollisionEnter(Fixture f2, Contact contact)
        {

        }

        public virtual void OnCollisionExit(Fixture f2)
        {

        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual Tuple<string, EditPane> GetPane()
        {
            return null;
        }
    }
}
