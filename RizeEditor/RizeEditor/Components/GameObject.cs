﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using RizeEditor.Managers;

namespace RizeEditor.Components
{
    class GameObject : IFocusable
    {
        private List<Component> Components;
        private float x, y;
        private float prevX, prevY;
        private float rotation;
        private float xOrig;
        private float yOrig;

        public float X { get { return x; } set { if (value != x) HasMoved = true; x = value; } }
        public float Y { get { return y; } set { if (value != y) HasMoved = true; y = value; } }
        public float Rotation { get { return rotation; } set { rotation = value; } }
        public float XOrig { get { return xOrig; } set { xOrig = value; } }
        public float YOrig { get { return yOrig; } set { yOrig = value; } }
        public Vector2 Position { get { return new Vector2(X, Y); } }
        public bool HasMoved { get; set; }
        public float PrevX { get { return prevX; } set { prevX = value; } }
        public float PrevY { get { return prevY; } set { prevY = value; } }
        public bool UpdateIt { get; set; }


        public GameObject()
        {
            Components = new List<Component>();
            x = 0;
            y = 0;
            rotation = 0;
            xOrig = 0;
            yOrig = 0;
            HasMoved = false;
            ObjectManager.AddObject(this);
            UpdateIt = true;
        }

        public void Update(GameTime gameTime)
        {
            int num = 0;
            foreach (Component c in Components)
            {
                if (c.ShouldUpdate) c.Update(gameTime);
                else num++;
            }
            if (num == Components.Count) UpdateIt = false;

            HasMoved = false;
            prevX = X;
            prevY = y;
        }

        public GameObject Destroy()
        {
            foreach (Component c in Components)
            {
                c.OnDestroy();
            }

            Components.Clear();

            return null;
        }

        public void Wake()
        {
            ObjectManager.WakeObject(this);
            UpdateIt = true;
        }

        public void Sleep()
        {
            UpdateIt = false;
        }

        #region Accessor/Setter Methods
        public void AddComponent(Component c)
        {
            Components.Add(c);
            c.Owner = this;
            c.OnInit();
        }
        public void RemoveComponent<T>()
        {
            foreach (Component c in Components)
            {
                if (c.GetType() == typeof(T))
                {
                    Components.Remove(c);
                    c.OnDestroy();
                    return;                 
                }
            }
            throw new ArgumentException("No component of type {" + typeof(T).ToString() + "} exists to remove");
        }
        public T GetComponent<T>()
        {
            foreach (Component c in Components)
            {
                if (c.GetType() == typeof(T)) { return (T)(object)c; }
            }
            return default(T);
        }
        public T GetComponentType<T>()
        {
            foreach (Component c in Components)
            {
                if (c is T) { return (T)(object)c; }
            }
            return default(T);
        }
        public List<Component> GetComponents()
        {
            return Components;
        }
        #endregion
    }
}
