﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using RizeEditor.Components;
using RizeEditor.Components.Tile;
using RizeEditor.Managers;

namespace RizeEditor
{

    class SemiBlockData
    {
        public string Name;
        public List<List<GameObject>> walls;
        public List<List<GameObject>> floors;
        public List<List<GameObject>> ground;
        public List<GameObject> objects;

        public SemiBlockData()
        {
            walls = new List<List<GameObject>>();
            floors = new List<List<GameObject>>();
            ground = new List<List<GameObject>>();
            objects = new List<GameObject>();
        }
    }

    class SemiBlock
    {
        private List<List<GameObject>> wallTiles;
        private List<List<GameObject>> floorTiles;
        private List<List<GameObject>> groundTiles;
        private List<GameObject> objects;

        public List<List<GameObject>> Walls { get { return wallTiles; } }
        public List<List<GameObject>> Floors { get { return floorTiles; } }
        public List<List<GameObject>> Ground { get { return groundTiles; } }
        public List<GameObject> Objects { get { return objects; } }

        const int dimension = 100;

        public int Dimension { get { return dimension; } }

        private string name;
        public string Name { get { return name; } set { name = value; } }

        public SemiBlock()
        {
            name = "";
            wallTiles = new List<List<GameObject>>();
            floorTiles = new List<List<GameObject>>();
            groundTiles = new List<List<GameObject>>();
            objects = new List<GameObject>();

            for (int i = 0; i < dimension; i++)
            {
                wallTiles.Add(new List<GameObject>());
                floorTiles.Add(new List<GameObject>());
                groundTiles.Add(new List<GameObject>());

                for (int j = 0; j < dimension; j++)
                {
                    wallTiles[i].Add(null);
                    floorTiles[i].Add(null);
                    groundTiles[i].Add(new GameObject());

                    //wallTiles[i][j].AddComponent(new CRenderTile(Managers.RenderLayers.Wall, 0));
                    //floorTiles[i][j].AddComponent(new CRenderTile(Managers.RenderLayers.Floor, 0));
                    groundTiles[i][j].AddComponent(new CRenderTile(Managers.RenderLayers.Ground, 1, true));

                    //wallTiles[i][j].X = i * TileManager.tileWidth + TileManager.tileWidth / 2;
                    //wallTiles[i][j].Y = j * TileManager.tileHeight + TileManager.tileHeight / 2;
                    //floorTiles[i][j].X = i * TileManager.tileWidth + TileManager.tileWidth / 2;
                    //floorTiles[i][j].Y = j * TileManager.tileHeight + TileManager.tileHeight / 2;
                    groundTiles[i][j].X = i * TileManager.tileWidth + TileManager.tileWidth / 2;
                    groundTiles[i][j].Y = j * TileManager.tileHeight + TileManager.tileHeight / 2;
                }
            }

            //The semi block should now be ready to go
        }

        public SemiBlock(SemiBlockData data)
        {
            name = data.Name;
            wallTiles = data.walls;
            floorTiles = data.floors;
            groundTiles = data.ground;
            objects = data.objects;
        }

        public void Destroy()
        {
            foreach (List<GameObject> l in groundTiles)
            {
                foreach (GameObject g in l)
                {
                    g.Destroy();
                }
            }

            foreach (List<GameObject> l in floorTiles)
            {
                foreach (GameObject g in l)
                {
                    if(g != null) g.Destroy();
                }
            }

            foreach (List<GameObject> l in wallTiles)
            {
                foreach (GameObject g in l)
                {
                    if(g != null) g.Destroy();
                }
            }

            foreach (GameObject g in objects)
            {
                g.Destroy();
            }


            groundTiles.Clear();
            floorTiles.Clear();
            wallTiles.Clear();
            objects.Clear();
        }

        public bool SetWallTile(GameObject wall, float X, float Y)
        {
            if (X >= 0 && Y >= 0 && X < dimension * TileManager.tileWidth && Y < dimension * TileManager.tileHeight)
            {
                int x = (int)X / TileManager.tileWidth;
                int y = (int)Y / TileManager.tileHeight;
                if (wallTiles[x][y] != null)
                    wallTiles[x][y].Destroy();

                wallTiles[x][y] = wall;
                wall.X = x * TileManager.tileWidth + TileManager.tileWidth / 2;
                wall.Y = y * TileManager.tileHeight + TileManager.tileHeight / 2;
                wall.GetComponent<CRenderTile>().autoOff = true;

                return true;
            }

            return false;
        }

        public void AddWall(GameObject wall)
        {
            int x = getTileX(wall.X);
            int y = getTileY(wall.Y);
            if (wallTiles[x][y] != null)
                wallTiles[x][y].Destroy();

            wallTiles[x][y] = wall;
            wall.GetComponent<CRenderTile>().autoOff = true;
        }

        public void AddFloor(GameObject floor)
        {
            int x = getTileX(floor.X);
            int y = getTileY(floor.Y);
            if (floorTiles[x][y] != null)
                floorTiles[x][y].Destroy();

            floorTiles[x][y] = floor;
            floor.GetComponent<CRenderTile>().autoOff = true;
        }

        public void AddObject(GameObject obj)
        {
            objects.Add(obj);
        }

        public bool SetFloorTile(GameObject floor, float X, float Y)
        {
            if (X >= 0 && Y >= 0 && X < dimension * TileManager.tileWidth && Y < dimension * TileManager.tileHeight)
            {
                int x = getTileX(X);
                int y = getTileY(Y);
                if (floorTiles[x][y] != null)
                    floorTiles[x][y].Destroy();

                floorTiles[x][y] = floor;
                floor.X = x * TileManager.tileWidth + TileManager.tileWidth / 2;
                floor.Y = y * TileManager.tileHeight + TileManager.tileHeight / 2;
                floor.GetComponent<CRenderTile>().autoOff = true;
                return true;
            }

            return false;
        }

        public void RemoveFloorTile(float X, float Y)
        {
            if (X >= 0 && Y >= 0 && X < dimension * TileManager.tileWidth && Y < dimension * TileManager.tileHeight)
            {
                int x = getTileX(X);
                int y = getTileY(Y);
                if (floorTiles[x][y] != null)
                {
                    floorTiles[x][y].Destroy();
                    floorTiles[x][y] = null;
                }
            }
        }

        public void RemoveWallTile(float X, float Y)
        {
            if (X >= 0 && Y >= 0 && X < dimension * TileManager.tileWidth && Y < dimension * TileManager.tileHeight)
            {
                int x = getTileX(X);
                int y = getTileY(Y);
                if (wallTiles[x][y] != null)
                {
                    wallTiles[x][y].Destroy();
                    wallTiles[x][y] = null;
                }
            }
        }

        private int getTileX(float X)
        {
            return (int)X / TileManager.tileWidth;
        }

        private int getTileY(float Y)
        {
            return (int)Y / TileManager.tileHeight;
        }
    }
}
