﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RizeEditor.Managers;
using RizeEditor.Utilities.UserInput;
using RizeEditor.Utilities.GUI;
using RizeEditor.Components;
using RizeEditor.Components.Tile;
using RizeEditor.Components.Lighting;
using RizeEditor.Factories;
using RizeEditor.Panes;
using RizeEditor.Screens;

using FarseerPhysics.Dynamics;
using FarseerPhysics;

namespace RizeEditor
{

    class EditorScreen
    {
        private Game game;
        private SemiBlock block;

        #region GUI Variables
        private List<GUI_Panel> panels;

        private GUI_Panel pnlBack;
        private GUI_SButton btnBack;

        //--MAIN MENU
        private GUI_Panel pnlMenu;
        private GUI_SButton btnTile;
        private GUI_SButton btnObject;
        private GUI_SButton btnSave;
        private GUI_SButton btnLoad;
        #endregion

        private TileScreen tScreen;
        private ObjectScreen oScreen;
        private SavingScreen sScreen;
        private Screen currentScreen;

        private GUI_ScrollMenu<string> LoadMenu;

        public EditorScreen(Game game)
        {
            this.game = game;
            block = new SemiBlock();
            panels = new List<GUI_Panel>();
            Init();
        }

        private void Init()
        {

            //--BACK MENU
            btnBack = new GUI_SButton(game, new Rectangle(20, game.GraphicsDevice.Viewport.Height - 50, 100, 35), Color.PowderBlue, "Back");
            btnBack.OnPress += OnBtnBack;
            pnlBack = new GUI_Panel();
            panels.Add(pnlBack);
            pnlBack.AddItem(btnBack);
            pnlBack.Enabled = false;

            //--MAIN MENU
            btnSave = new GUI_SButton(game, new Rectangle(20, 20, 100, 35), Color.PowderBlue, "Save");
            btnSave.OnPress += OnBtnSave;
            btnLoad = new GUI_SButton(game, new Rectangle(btnSave.Bounds.Right + 10, btnSave.Bounds.Y, 100, 35), Color.PowderBlue, "Load");
            btnLoad.OnPress += OnBtnLoad;
            btnTile = new GUI_SButton(game, new Rectangle(20, 150, 100, 35), Color.PowderBlue, "Tiles");
            btnTile.OnPress += OnBtnTile;
            btnObject = new GUI_SButton(game, new Rectangle(btnTile.Bounds.X, btnTile.Bounds.Bottom + 15, 100, 35), Color.PowderBlue, "Objects");
            btnObject.OnPress += OnBtnObject;
            pnlMenu = new GUI_Panel();
            panels.Add(pnlMenu);
            pnlMenu.AddItem(btnSave);
            pnlMenu.AddItem(btnLoad);
            pnlMenu.AddItem(btnTile);
            pnlMenu.AddItem(btnObject);

            tScreen = new TileScreen(block);
            oScreen = new ObjectScreen(block);
            sScreen = new SavingScreen(block);
            currentScreen = null;

            LoadMenu = new GUI_ScrollMenu<string>(game, new Rectangle(game.GraphicsDevice.Viewport.TitleSafeArea.Center.X - 150, game.GraphicsDevice.Viewport.TitleSafeArea.Center.Y - 150, 300, 300), Color.Gray);
            LoadMenu.Enabled = false;
            LoadMenu.OnSelected += OnLoadSelected;
            

        }

        #region Main Menu Handlers
        private void OnBtnSave(object o, EventArgs e)
        {
            pnlMenu.Enabled = false;
            LoadMenu.Enabled = false;
            if (currentScreen != null)
                currentScreen.Close();

            currentScreen = sScreen;
            currentScreen.Open();

            pnlBack.Enabled = true;
        }

        private void OnBtnLoad(object o, EventArgs e)
        {
            LoadMenu.Enabled = true;
            LoadMenu.Clear();
            foreach (string s in Global.BlockSaver.SavedFiles)
            {
                LoadMenu.AddEntry(s, s);
            }
        }

        private void OnLoadSelected(string name)
        {
            //Load the semiblock in, dispose of the current empty one, and add the new one in, making sure to add it to screens.
            block.Destroy();
            block = new SemiBlock(Global.BlockSaver.LoadBlock(name));
            tScreen.Block = block;
            oScreen.Block = block;
            LoadMenu.Enabled = false;
        }

        private void OnBtnTile(object o, EventArgs e)
        {
            pnlMenu.Enabled = false;
            LoadMenu.Enabled = false;

            if (currentScreen != null)
                currentScreen.Close();

            currentScreen = tScreen;
            currentScreen.Open();

            pnlBack.Enabled = true;
        }

        private void OnBtnObject(object o, EventArgs e)
        {
            pnlMenu.Enabled = false;
            LoadMenu.Enabled = false;

            if (currentScreen != null)
                currentScreen.Close();

            currentScreen = oScreen;
            currentScreen.Open();

            pnlBack.Enabled = true;
        }
        #endregion

        #region BackButton Handlers
        private void OnBtnBack(object o, EventArgs e)
        {
            currentScreen.Close();
            currentScreen = null;
            
            //Go back to main menu
            pnlMenu.Enabled = true;
            pnlBack.Enabled = false;
        }
        #endregion        

        public void Update(GameTime gameTime)
        {

            //Move the camera
            //----
            Vector2 moveTemp = Vector2.Zero;
            if (Input.IsDown(Keys.W)) moveTemp.Y -= 1;
            if (Input.IsDown(Keys.S)) moveTemp.Y += 1;
            if (Input.IsDown(Keys.A)) moveTemp.X -= 1;
            if (Input.IsDown(Keys.D)) moveTemp.X += 1;

            RenderManager.camera.MovePast(moveTemp * (400f * (float)gameTime.ElapsedGameTime.TotalSeconds));
            //----

            if (Input.IsDown(Keys.PageUp)) RenderManager.LightEngine.Bluriness += 0.01f;
            if (Input.IsDown(Keys.PageDown)) RenderManager.LightEngine.Bluriness -= 0.01f;


            #region Handling Keyboard
            if (Input.IsDown(Keys.Up))
            {
                RenderManager.LightEngine.AmbientColor = new Color(RenderManager.LightEngine.AmbientColor.R + 1, RenderManager.LightEngine.AmbientColor.G + 1, RenderManager.LightEngine.AmbientColor.B + 1);
            }

            if (Input.IsDown(Keys.Down))
            {
                RenderManager.LightEngine.AmbientColor = new Color(RenderManager.LightEngine.AmbientColor.R - 1, RenderManager.LightEngine.AmbientColor.G - 1, RenderManager.LightEngine.AmbientColor.B - 1);
            }
            #endregion

            pnlMenu.Update(gameTime);
            pnlBack.Update(gameTime);
            LoadMenu.Update(gameTime);
            if(currentScreen != null) currentScreen.Update(gameTime);
        }

        public void Draw(SpriteBatch SB)
        {
            pnlMenu.Draw(SB);
            pnlBack.Draw(SB);
            LoadMenu.Draw(SB);
            if(currentScreen != null) currentScreen.Draw(SB);
        }
    }
}
