﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RizeEditor
{
    public interface IFocusable
    {
        Vector2 Position { get; }
    }

    public class CameraPos : IFocusable
    {
        public Vector2 Position { get; set; }

        public CameraPos()
        {
            Position = Vector2.Zero;
        }
    }

    public class Camera2D:GameComponent
    {

        private const float zoomUpperLimit = 1.5f;
        private const float zoomLowerLimit = .5f;
        private const float followSpeed = 6.5f;

        private float _zoom;
        private Matrix _transform;
        private Vector2 position;
        private Vector2 posRemainder;
        public Vector2 Origin { get; set; }
        private float _rotation;
        private int _worldWidth;
        private int _worldHeight;

        private IFocusable focus;
        public IFocusable Focus { get { return focus; } set { focus = value;} }


        public Camera2D(Game game, int worldWidth,
           int worldHeight, IFocusable theFocus)
            :base(game)
        {
            if (theFocus == null)
            {
                focus = null;
                position = Vector2.Zero;
            }
            else
            {
                focus = theFocus;
                position = focus.Position;
            }
            

            Origin = new Vector2(Game.GraphicsDevice.Viewport.Width / 2.0f, Game.GraphicsDevice.Viewport.Height / 2.0f);
            _zoom = 1f;
            _rotation = 0.0f;
            posRemainder = Vector2.Zero;

            if (worldWidth == 0) _worldWidth = int.MaxValue;
            else _worldWidth = worldWidth;
            if (worldHeight == 0) _worldHeight = int.MaxValue;
            else _worldHeight = worldHeight;
        }

        #region Properties

        public float Zoom
        {
            get { return _zoom; }
            set
            {
                _zoom = value;
                if (_zoom < zoomLowerLimit)
                    _zoom = zoomLowerLimit;
                if (_zoom > zoomUpperLimit)
                    _zoom = zoomUpperLimit;
            }
        }

        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }

        public void Move(Vector2 amount)
        {
            if(focus == null) Center += amount;
        }

        public void MovePast(Vector2 amount)
        {
            if (focus == null) position += amount;
        }

        public Vector2 Position
        {
            get
            {
                return new Vector2(position.X - Game.GraphicsDevice.Viewport.Width / 2, position.Y - Game.GraphicsDevice.Viewport.Height / 2);
            }
            set
            {
                position = value;
            }
        }

        public Vector2 Center
        {
            get { return position; }
            set
            {
                float leftBarrier = (float)Game.GraphicsDevice.Viewport.Width *
                       .5f / _zoom;
                float rightBarrier = _worldWidth -
                       (float)Game.GraphicsDevice.Viewport.Width * .5f / _zoom;
                float topBarrier = _worldHeight -
                       (float)Game.GraphicsDevice.Viewport.Height * .5f / _zoom;
                float bottomBarrier = (float)Game.GraphicsDevice.Viewport.Height *
                       .5f / _zoom;
                position = value;
                if (position.X < leftBarrier)
                    position.X = leftBarrier;
                if (position.X > rightBarrier)
                    position.X = rightBarrier;
                if (position.Y > topBarrier)
                    position.Y = topBarrier;
                if (position.Y < bottomBarrier)
                    position.Y = bottomBarrier;
            }
        }

        #endregion

        public Matrix GetTransformation()
        {
            _transform =
               Matrix.CreateTranslation(new Vector3(-position.X, -position.Y, 0)) *
               Matrix.CreateRotationZ(Rotation) *
               Matrix.CreateScale(new Vector3(Zoom, Zoom, 1)) *
               Matrix.CreateTranslation(new Vector3(Game.GraphicsDevice.Viewport.Width * 0.5f,
                   Game.GraphicsDevice.Viewport.Height * 0.5f, 0));

            return _transform;
        }

        public Matrix GetViewMatrix(Vector2 parallax)
        {
            return Matrix.CreateTranslation(new Vector3(-Position * parallax, 0.0f)) *
                Matrix.CreateTranslation(new Vector3(-Origin, 0.0f)) *
                Matrix.CreateRotationZ(Rotation) *
                Matrix.CreateScale(Zoom, Zoom, 1) *
                Matrix.CreateTranslation(new Vector3(Origin, 0.0f));
        }

        public override void Update(GameTime gameTime)
        {
            if (Focus != null)
            {
                position += posRemainder;
                var delta = (float)gameTime.ElapsedGameTime.TotalSeconds;
                position.X += (focus.Position.X - position.X) * followSpeed * delta;
                position.Y += (focus.Position.Y - position.Y) * followSpeed * delta;
                posRemainder.X = position.X % 1;
                posRemainder.Y = position.Y % 1;
                //position.X = (float)Math.Floor(position.X);
                //position.Y = (float)Math.Floor(position.Y);
                Center = new Vector2((float)Math.Floor(position.X), (float)Math.Floor(position.Y));
            }
        }
    }
}
