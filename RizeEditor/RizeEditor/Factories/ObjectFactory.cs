﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RizeEditor.Components;
using RizeEditor.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using FarseerPhysics.Dynamics;

namespace RizeEditor.Factories
{
    class ObjectFactory
    {
        private static Game game;
        private const string O = "Objects/";

        public ObjectFactory(Game g)
        {
            game = g;
        }

        public static bool IsValidID(int ID)
        {
            switch (ID)
            {
                case 101:
                case 102:
                case 201:
                case 202:
                case 203:
                case 301:
                case 302:
                case 303:
                case 401:
                    return true;
                default:
                    return false;
            }
        }

        public static GameObject GetSensor(int ID, Vector2 position)
        {
            GameObject newO = new GameObject();
            CRenderSprite newOS;
            switch (ID)
            {
                case 101:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenCouch"), RenderLayers.Wall);
                    break;
                case 102:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenSmallCouch"), RenderLayers.Wall);
                    break;
                case 201:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenToilet"), RenderLayers.Wall);
                    break;
                case 202:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenSink"), RenderLayers.Wall);
                    break;
                case 203:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenTub"), RenderLayers.Wall);
                    break;
                case 301:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZBed"), RenderLayers.Wall);
                    break;
                case 302:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenDresser"), RenderLayers.Wall);
                    break;
                case 303:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenNightStand"), RenderLayers.Wall);
                    break;
                case 401:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenStove"), RenderLayers.Wall);
                    break;
                default:
                    return null;
            }

            newO.AddComponent(new CPhysRecSensor(newOS.Texture.Bounds.Width, newOS.Texture.Bounds.Height, position, Category.Cat4, Category.All));
            newO.AddComponent(newOS);

            return newO;
        }

        public static GameObject GetObject(int ID, Vector2 position, float rotation)
        {
            GameObject newO = new GameObject();
            CRenderSprite newOS;
            switch (ID)
            {
                case 101:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenCouch"), RenderLayers.Wall);
                    break;
                case 102:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenSmallCouch"), RenderLayers.Wall);
                    break;
                case 201:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenToilet"), RenderLayers.Wall);
                    break;
                case 202:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenSink"), RenderLayers.Wall);
                    break;
                case 203:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenTub"), RenderLayers.Wall);
                    break;
                case 301:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZBed"), RenderLayers.Wall);
                    break;
                case 302:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenDresser"), RenderLayers.Wall);
                    break;
                case 303:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenNightStand"), RenderLayers.Wall);
                    break;
                case 401:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenStove"), RenderLayers.Wall);
                    break;
                default:
                    return null;
            }

            newO.AddComponent(new CPhysRec(newOS.Texture.Bounds.Width, newOS.Texture.Bounds.Height, 0f, position, rotation, Category.Cat4, Category.All, BodyType.Static));
            newO.AddComponent(newOS);

            return newO;
        }

        public static GameObject GetByID(int ID, Vector2? position, bool Sensor)
        {
            GameObject newO = new GameObject();
            CRenderSprite newOS;
            switch (ID)
            {
                case 101:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenCouch"), RenderLayers.Wall);
                    break;
                case 102:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenSmallCouch"), RenderLayers.Wall);
                    break;
                case 201:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenToilet"), RenderLayers.Wall);
                    break;
                case 202:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenSink"), RenderLayers.Wall);
                    break;
                case 203:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenTub"), RenderLayers.Wall);
                    break;
                case 301:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZBed"), RenderLayers.Wall);
                    break;
                case 302:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenDresser"), RenderLayers.Wall);
                    break;
                case 303:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenNightStand"), RenderLayers.Wall);
                    break;
                case 401:
                    newOS = new CRenderSprite(game.Content.Load<Texture2D>(O + "ZGenStove"), RenderLayers.Wall);
                    break;
                default:
                    return null;
            }

            if (!Sensor)
            {
                if (position.HasValue)
                    newO.AddComponent(new CPhysRec(newOS.Texture.Bounds.Width, newOS.Texture.Bounds.Height, 0f, position.Value, 0f, Category.Cat4, Category.All, BodyType.Static));
                else newO.AddComponent(new CPhysRec(newOS.Texture.Bounds.Width, newOS.Texture.Bounds.Height, 0f, Vector2.Zero, 0f, Category.Cat4, Category.All, BodyType.Static));
                newO.AddComponent(newOS);

            }
            else
            {
                if (position.HasValue)
                    newO.AddComponent(new CPhysRec(newOS.Texture.Bounds.Width, newOS.Texture.Bounds.Height, 0f, position.Value, 0f, Category.Cat4, Category.All, BodyType.Static));
                else newO.AddComponent(new CPhysRec(newOS.Texture.Bounds.Width, newOS.Texture.Bounds.Height, 0f, Vector2.Zero, 0f, Category.Cat4, Category.All, BodyType.Static));

                newO.GetComponent<CPhysRec>().Body.Enabled = false;

                if (position.HasValue)
                    newO.AddComponent(new CPhysRecSensor(newOS.Texture.Bounds.Width, newOS.Texture.Bounds.Height, position.Value, Category.Cat4, Category.All));
                else newO.AddComponent(new CPhysRecSensor(newOS.Texture.Bounds.Width, newOS.Texture.Bounds.Height, Vector2.Zero, Category.Cat4, Category.All));
                newO.AddComponent(newOS);
            }


            return newO;
        }
    }
}
