﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using EasyStorage;
using RizeEditor.Managers;

namespace RizeEditor
{
    static class Global
    {
        public static Game Game;
        public static IAsyncSaveDevice SaveDevice;
        public static BlockManager BlockSaver;
    }
}
