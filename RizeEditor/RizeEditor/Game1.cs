using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RizeEditor.Components;
using RizeEditor.Utilities.UserInput;
using RizeEditor.Managers;
using FarseerPhysics.Dynamics;
using FarseerPhysics;
using RizeEditor.Utilities.GUI;
using RizeEditor.Factories;
using EasyStorage;


namespace RizeEditor
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        FrameCounter fCounter;

        EditorScreen Screen;

        public static Random random;
        GUI_SButton testButton;
        RasterizerState scissorState;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.IsFixedTimeStep = false;
            graphics.SynchronizeWithVerticalRetrace = false;
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1280;
            graphics.IsFullScreen = false;
            this.IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Global.Game = this;
            this.Components.Add(fCounter = new FrameCounter(this));
            fCounter.Type = FrameCounterType.OnScreen;
            Input input = new Input(this, false);
            random = new Random();
            TileManager tileManager = new TileManager(this, Content.Load<Texture2D>("TileSheet"));
            
            
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            SharedSaveDevice sSD = new SharedSaveDevice();
            this.Components.Add(sSD);
            Global.SaveDevice = sSD;
            sSD.DeviceSelectorCanceled +=
                (s, e) => e.Response = SaveDeviceEventResponse.Force;
            sSD.DeviceDisconnected +=
                (s, e) => e.Response = SaveDeviceEventResponse.Force;
            sSD.DeviceSelected += (s, e) => {Global.SaveDevice = ((SaveDevice) s);};
            sSD.PromptForDevice();

            Global.BlockSaver = new BlockManager();
            PhysicsManager physicsManager = new PhysicsManager(this);
            RenderManager renderManager = new RenderManager(this, GraphicsDevice);
            ObjectManager objectManager = new ObjectManager(this);
            ObjectFactory objectFactory = new ObjectFactory(this);


            Screen = new EditorScreen(this);

            Input.ActionAddAll("Up", Keys.W);
            Input.ActionAddAll("Down", Keys.S);
            Input.ActionAddAll("Left", Keys.A);
            Input.ActionAddAll("Right", Keys.D);

            scissorState = new RasterizerState() { ScissorTestEnable = true };

            testButton = new GUI_SButton(this, new Rectangle(300, 300, 150, 100), Color.Maroon, "button");
            testButton.OnPress += (s, e) =>
            {
                Console.Write("");
                testButton.Bounds = new Rectangle(testButton.Bounds.X + 10, testButton.Bounds.Y, testButton.Bounds.Width, testButton.Bounds.Height);
                return;
            };
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            Profiler.Start("Update");
            Console.SetCursorPosition(0, 0);


            if (Input.IsPressed(PlayerIndex.One, Buttons.Back) || Input.IsPressed(Keys.Escape))
                this.Exit();


            Screen.Update(gameTime);
            Global.BlockSaver.Update();
            base.Update(gameTime);
            Profiler.End("Update");
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            Profiler.Start("Draw");
            GraphicsDevice.Clear(Color.CornflowerBlue);
            RenderManager.Draw(gameTime);
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, scissorState);
            Screen.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
            Profiler.End("Draw");
        }
    }
}
