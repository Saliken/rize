﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace RizeEditor
{

    public enum FrameCounterType
    {
        OnScreen,
        WindowTitle,
        Console
    }

    public class FrameCounter : DrawableGameComponent
    {
        ContentManager content;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;

        int frameRate = 0;
        int frameCounter = 0;
        TimeSpan elapsedTime = TimeSpan.Zero;
        public FrameCounterType Type;



        public FrameCounter(Game game)
            : base(game)
        {
            Type = FrameCounterType.OnScreen;
            content = new ContentManager(game.Services);
            
        }


        public override void Update(GameTime gameTime)
        {
            elapsedTime += gameTime.ElapsedGameTime;

            if (elapsedTime > TimeSpan.FromSeconds(1))
            {
                elapsedTime -= TimeSpan.FromSeconds(1);
                frameRate = frameCounter;
                frameCounter = 0;
            }
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            spriteFont = Game.Content.Load<SpriteFont>("GenericFont");
            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            frameCounter++;
            string fps = string.Format("fps: {0}", frameRate);

            switch (Type)
            {
                case FrameCounterType.OnScreen:
                    spriteBatch.Begin();
                    spriteBatch.DrawString(spriteFont, fps, new Vector2(33, 33), Color.Black);
                    spriteBatch.DrawString(spriteFont, fps, new Vector2(32, 32), Color.White);
                    spriteBatch.End();
                    break;
                case FrameCounterType.Console:
                    Console.WriteLine(fps);
                    break;
                case FrameCounterType.WindowTitle:
                    this.Game.Window.Title = fps;
                    break;
            }
            
            
        }
    }
}
