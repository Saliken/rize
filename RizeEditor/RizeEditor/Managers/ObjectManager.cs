﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RizeEditor.Components;
using Microsoft.Xna.Framework;

namespace RizeEditor.Managers
{
    class ObjectManager:GameComponent
    {
        private static List<GameObject> objects;
        private static List<GameObject> transitionList;
        private static List<GameObject> objectsNotUpdate;
        private static bool Initialized = false;

        public ObjectManager(Game game)
            :base(game)
        {
            if (!Initialized)
            {
                objects = new List<GameObject>();
                objectsNotUpdate = new List<GameObject>();
                transitionList = new List<GameObject>();
                game.Components.Add(this);
                Initialized = true;
            }
        }

        public override void Update(GameTime gameTime)
        {
            foreach (GameObject o in objects)
            {
                if (o.UpdateIt) o.Update(gameTime);
                else transitionList.Add(o);
            }

            foreach (GameObject o in transitionList)
            {
                objectsNotUpdate.Add(o);
                objects.Remove(o);
            }

            transitionList.Clear();
            base.Update(gameTime);
        }

        public static void WakeObject(GameObject obj)
        {
            objects.Add(obj);
            objectsNotUpdate.Remove(obj);
        }

        public static void AddObject(GameObject obj)
        {
            objects.Add(obj);
        }
    }
}
