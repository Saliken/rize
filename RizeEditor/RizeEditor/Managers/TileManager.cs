﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RizeEditor.Managers
{
    class TileManager:GameComponent
    {
        private static Texture2D tileSheet;
        public static Texture2D TileSheet { get { return tileSheet; } }
        private static Dictionary<int, Rectangle> SourceRecs;
        private static int maxID;

        public const int tileWidth = 32;
        public const int tileHeight = 32;

        public TileManager(Game game, Texture2D tileTex)
            :base(game)
        {
            tileSheet = tileTex;
            SourceRecs = new Dictionary<int, Rectangle>();
            game.Components.Add(this);
        }

        public override void Initialize()
        {
            //Initialize source recs
            int width = tileSheet.Width / tileWidth;
            int height = tileSheet.Height / tileHeight;

            for (int i = 1; i <= width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    SourceRecs.Add((j * width) + i, new Rectangle((i-1) * tileWidth, j * tileHeight, tileWidth, tileHeight));
                }
            }

            maxID = width * height;

            base.Initialize();
        }

        public static Rectangle GetSourceFromID(int id)
        {
            if (id > 0 && id <= maxID)
            {
                return SourceRecs[id];
            }
            else return Rectangle.Empty;
        }

    }
}
