﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using FarseerPhysics;

namespace RizeEditor.Managers
{
    class PhysicsManager:GameComponent
    {
        public static World PhysicsWorld;
        private static bool Initialized = false;

        public PhysicsManager(Game game)
            :base(game)
        {
            if (!Initialized)
            {
                if (PhysicsWorld == null) PhysicsWorld = new World(Vector2.Zero);
                game.Components.Add(this);
                ConvertUnits.SetDisplayUnitToSimUnitRatio(16);
                Initialized = true;
            }
        }

        public override void Update(GameTime gametime)
        {
            PhysicsWorld.Step(1.0f / 60.0f);

            base.Update(gametime);
        }
    }
}
