﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EasyStorage;
using System.IO;
using RizeEditor.Components.Tile;
using RizeEditor.Components;

namespace RizeEditor.Managers
{
    class BlockManager
    {
        private const string blockContainer = "SemiBlocks";
        private bool Initialized = false;
        private List<string> blockNames;
        public List<string> SavedFiles { get { if (Initialized) { return blockNames; } else { Init(); return blockNames; } } }

        private bool manifestUpdated;

        public BlockManager()
        {
            blockNames = new List<string>();
            manifestUpdated = false;
        }

        private void Init()
        {
            //As Initialization the manifest needs to be loaded..
            //.. and a list of filenames compiled. If no manifest..
            //.. can be found then a new one must be created.
            if (Global.SaveDevice.FileExists(blockContainer, "Manifest.Man"))
            {
                blockNames.Clear(); //Make sure I'm not loading in already existing names.

                Global.SaveDevice.Load(blockContainer, "Manifest.Man",
                    stream =>
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            //Load the header
                            reader.ReadLine();

                            //Load the list of file names
                            string name;
                            while ((name = reader.ReadLine()) != null)
                            {
                                blockNames.Add(name);
                            }
                        }
                    });
            }
            else if (Global.SaveDevice.IsReady)
            {
                Global.SaveDevice.SaveAsync(blockContainer, "Manifest.man",
                    stream =>
                    {
                        using (StreamWriter writer = new StreamWriter(stream))
                        {
                            writer.WriteLine("SemiBlock Manifest");
                        }
                    });
            }


            Initialized = true;
        }

        public void Update()
        {
            SaveManifest();
        }

        private void SaveManifest()
        {
            if (manifestUpdated && Global.SaveDevice.IsReady) //The manifest should be saved
            {
                Global.SaveDevice.Save(blockContainer, "Manifest.Man",
                    stream =>
                    {
                        using (StreamWriter writer = new StreamWriter(stream))
                        {
                            writer.WriteLine("SemiBlock Manifest");

                            foreach (string s in blockNames)
                            {
                                writer.WriteLine(s);
                            }
                        }
                    });

                manifestUpdated = false;
            }
        }


        /// <summary>
        /// Method to update the manifest if a new file has been saved
        /// </summary>
        /// <param name="name"></param>
        private void UpdateManifest(string name)
        {
            foreach (string s in blockNames)
            {
                if (name == s)
                {
                    return;
                }
            }

            //If the names don't match then add it to the list and flag the manifest
            blockNames.Add(name);
            manifestUpdated = true;
        }

        public void LoadAllBlocks()
        {

        }

        public SemiBlockData LoadBlock(string name)
        {
            SemiBlockData data = new SemiBlockData();

            if (!Initialized) Init();
            if (Global.SaveDevice.FileExists(blockContainer, name))
            {
                Global.SaveDevice.Load(blockContainer, name,
                    stream =>
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            //Ground Tiles
                            List<List<GameObject>> groundT = new List<List<GameObject>>();
                            List<List<GameObject>> floorT = new List<List<GameObject>>();
                            List<List<GameObject>> wallT = new List<List<GameObject>>();
                            reader.ReadLine();
                            for (int i = 0; i < 100; i++)
                            {
                                groundT.Add(new List<GameObject>());
                                for (int y = 0; y < 100; y++)
                                {
                                    GameObject newG = new GameObject();
                                    int ID = int.Parse(reader.ReadLine());
                                    float X = float.Parse(reader.ReadLine());
                                    float Y = float.Parse(reader.ReadLine());
                                    newG.AddComponent(new CRenderTile(RenderLayers.Ground, ID, true));
                                    newG.X = X;
                                    newG.Y = Y;
                                    groundT[i].Add(newG);
                                }
                            }

                            //Floor
                            reader.ReadLine();
                            for (int i = 0; i < 100; i++)
                            {
                                floorT.Add(new List<GameObject>());
                                for (int y = 0; y < 100; y++)
                                {

                                    int ID = int.Parse(reader.ReadLine());
                                    int Group = int.Parse(reader.ReadLine());
                                    float X = float.Parse(reader.ReadLine());
                                    float Y = float.Parse(reader.ReadLine());
                                    if (ID != 0)
                                    {
                                        GameObject newG = new GameObject();
                                        newG.AddComponent(new CRenderTile(RenderLayers.Ground, ID, true));
                                        CTileGroup ct = new CTileGroup();
                                        newG.AddComponent(ct);
                                        ct.Group = Group;
                                        newG.X = X;
                                        newG.Y = Y;
                                        floorT[i].Add(newG);
                                    }
                                    else floorT[i].Add(null);
                                }
                            }

                            //Wall
                            reader.ReadLine();
                            for (int i = 0; i < 100; i++)
                            {
                                wallT.Add(new List<GameObject>());
                                for (int y = 0; y < 100; y++)
                                {

                                    int ID = int.Parse(reader.ReadLine());
                                    int Group = int.Parse(reader.ReadLine());
                                    float X = float.Parse(reader.ReadLine());
                                    float Y = float.Parse(reader.ReadLine());
                                    if (ID != 0)
                                    {
                                        GameObject newG = new GameObject();
                                        newG.AddComponent(new CRenderTile(RenderLayers.Ground, ID, true));
                                        CTileGroup ct = new CTileGroup();
                                        newG.AddComponent(ct);
                                        ct.Group = Group;
                                        newG.X = X;
                                        newG.Y = Y;
                                        wallT[i].Add(newG);
                                    }
                                    else wallT[i].Add(null);
                                }
                            }

                            data.ground = groundT;
                            data.floors = floorT;
                            data.walls = wallT;
                        }
                    });
                return data;
            }
            else
            {
                return null;
            }
        }

        public void SaveBlock(SemiBlock block)
        {
            if(!Initialized) Init();
            if (Global.SaveDevice.IsReady)
            {
                Global.SaveDevice.SaveAsync(blockContainer, block.Name + ".blk",
                stream =>
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        //Write out the block data
                        //Start by saving the tiles.
                        //--Ground tiles--
                        writer.WriteLine("Ground Tiles");
                        for (int i = 0; i < block.Ground.Count; i++)
                        {
                            for (int j = 0; j < block.Ground[0].Count; j++)
                            {
                                int ID = block.Ground[i][j].GetComponent<CRenderTile>().ID;
                                writer.WriteLine(ID);
                                writer.WriteLine(block.Ground[i][j].X);
                                writer.WriteLine(block.Ground[i][j].Y);
                                writer.Flush();
                            }
                        }

                        //--Floor tiles--
                        writer.WriteLine("Floor Tiles");
                        for (int i = 0; i < block.Floors.Count; i++)
                        {
                            for (int j = 0; j < block.Floors[0].Count; j++)
                            {
                                if (block.Floors[i][j] != null)
                                {
                                    int ID = block.Floors[i][j].GetComponent<CRenderTile>().ID;
                                    int Group = block.Floors[i][j].GetComponent<CTileGroup>().Group;
                                    writer.WriteLine(ID);
                                    writer.WriteLine(Group);
                                    writer.WriteLine(block.Floors[i][j].X);
                                    writer.WriteLine(block.Floors[i][j].Y);
                                    writer.Flush();
                                }
                                else
                                {
                                    writer.WriteLine(0);
                                    writer.WriteLine(0);
                                    writer.WriteLine(0);
                                    writer.WriteLine(0);
                                    writer.Flush();
                                }
                            }
                        }

                        //--Wall tiles--
                        writer.WriteLine("Wall Tiles");
                        for (int i = 0; i < block.Walls.Count; i++)
                        {
                            for (int j = 0; j < block.Walls[0].Count; j++)
                            {
                                if (block.Walls[i][j] != null)
                                {
                                    int ID = block.Walls[i][j].GetComponent<CRenderTile>().ID;
                                    int Group = block.Walls[i][j].GetComponent<CTileGroup>().Group;
                                    writer.WriteLine(ID);
                                    writer.WriteLine(Group);
                                    writer.WriteLine(block.Walls[i][j].X);
                                    writer.WriteLine(block.Walls[i][j].Y);
                                    writer.Flush();
                                }
                                else
                                {
                                    writer.WriteLine(0);
                                    writer.WriteLine(0);
                                    writer.WriteLine(0);
                                    writer.WriteLine(0);
                                    writer.Flush();
                                }
                            }
                            
                        }

                        //Then save the objects and their components
                        /*writer.WriteLine("Objects");
                        for (int i = 0; i < block.Objects.Count; i++)
                        {

                        }*/
                    }
                });

                UpdateManifest(block.Name + ".blk");
            }
        }
    }
}
