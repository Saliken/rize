﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RizeEditor.Components;
using RizeEditor.Structures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Krypton;

namespace RizeEditor.Managers
{
    public enum RenderLayers
    {
        Ground,
        Floor,
        Wall,
        Tree,
        Tree2,
        Tree3,
        Tree4
    }

    class RenderManager:GameComponent
    {
        public static Camera2D camera;
        public static KryptonEngine LightEngine;
        private static SpriteBatch spriteBatch;
        private static bool Initialize = false;
        public static GraphicsDevice graphicsDevice;

        private static Dictionary<RenderLayers, SpatialHash2D> Objects;
        const int CellSize = 100;
        private static int steps;

        public RenderManager(Game game, GraphicsDevice gd)
            :base(game)
        {
            if (!Initialize)
            {
                graphicsDevice = gd;
                LightEngine = new KryptonEngine(game, "Lighting");
                LightEngine.Initialize();
                LightEngine.CullMode = CullMode.None;
                LightEngine.SpriteBatchCompatablityEnabled = true;
                LightEngine.AmbientColor = new Color(125, 125, 125);

                spriteBatch = new SpriteBatch(game.GraphicsDevice);
                camera = new Camera2D(game, 0, 0, null);
                game.Components.Add(camera);
                Objects = new Dictionary<RenderLayers, SpatialHash2D>();
                foreach (RenderLayers layer in Enum.GetValues(typeof(RenderLayers)))
                {
                    Objects.Add(layer, new SpatialHash2D(CellSize));
                }
                steps = (int)Math.Ceiling((Game.GraphicsDevice.Viewport.Width / 2f) / CellSize);
                Initialize = true;
                game.Components.Add(this);
            }
        }

        public static void UpdateObject(GameObject obj)
        {
            Objects[obj.GetComponentType<CRender>().Layer].RemoveObjectMoved(obj);
            Objects[obj.GetComponentType<CRender>().Layer].AddObject(obj);
        }

        public static void AddRenderComponent(RenderLayers layer, CRender component)
        {
            Objects[layer].AddObject(component.Owner);
        }

        public static void RemoveRenderComponent(RenderLayers layer, CRender component)
        {
            Objects[layer].RemoveObject(component.Owner);
        }

        public override void Update(GameTime gameTime)
        {
            steps = (int)Math.Ceiling((Game.GraphicsDevice.Viewport.Width / 2f) / CellSize);
            base.Update(gameTime);
        }

        public static void Draw(GameTime gt)
        {
            LightEngine.Matrix = camera.GetTransformation();
            LightEngine.LightMapPrepare();
            graphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, camera.GetTransformation());
            foreach (RenderLayers layer in Enum.GetValues(typeof(RenderLayers)))
            {
                foreach (GameObject go in Objects[layer].GetObjectsAround(camera.Center, steps))
                {

                    go.GetComponentType<CRender>().Draw(spriteBatch);
                }
            }
            spriteBatch.End();
            LightEngine.Draw(gt);
        }

        public static Texture2D CreateTex(Game game, int Width, int Height, Color Color)
        {
            
            Texture2D tempTex = new Texture2D(game.GraphicsDevice, Width, Height);
            Color[] fillColor = new Color[Width * Height];
            for (int i = 0; i < fillColor.Length; i++)
            {
                fillColor[i] = Color;
            }
            tempTex.SetData<Color>(fillColor);
            return tempTex;
        }
    }
}
