﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RizeEditor.Utilities.GUI;
using RizeEditor.Utilities.UserInput;
using RizeEditor.Managers;
using RizeEditor.Components;
using RizeEditor.Components.Lighting;
using RizeEditor.Components.Tile;
using FarseerPhysics.Dynamics;

namespace RizeEditor.Screens
{
    class TileScreen : Screen
    {
        private GUI_SButton btnWall;
        private GUI_SButton btnWallEraser;
        private GUI_SButton btnFloor;
        private GUI_SButton btnFloorEraser;
        private GUI_STextBox txtTileNum;
        private GUI_Panel panel;

        private int tileGroup;
        private GameObject Brush;
        private SemiBlock block;
        public SemiBlock Block { get { return block; } set { block = value; } }

        enum state
        {
            Idle,
            Wall,
            WallErase,
            Floor,
            FloorErase,
        }

        private state State;

        public TileScreen(SemiBlock block)
        {

            this.block = block;

            txtTileNum = new GUI_STextBox(Global.Game, new Rectangle(Global.Game.Window.ClientBounds.Width / 2, Global.Game.Window.ClientBounds.Height - 200, 100, 35), Color.LightGreen);
            txtTileNum.OnTextChange += OnTxtTileNumChange;
            txtTileNum.Text = "1";

            tileGroup = 1;

            btnWall = new GUI_SButton(Global.Game, new Rectangle(20, 150, 100, 35), Color.PowderBlue, "Wall");
            btnWall.OnPress += OnBtnWall;
            btnFloor = new GUI_SButton(Global.Game, new Rectangle(btnWall.Bounds.X, btnWall.Bounds.Bottom + 10, btnWall.Bounds.Width, btnWall.Bounds.Height), Color.PowderBlue, "Floor");
            btnFloor.OnPress += OnBtnFloor;
            btnWallEraser = new GUI_SButton(Global.Game, new Rectangle(btnWall.Bounds.Right + 10, btnWall.Bounds.Y, 150, btnWall.Bounds.Height), Color.PowderBlue, "Wall Eraser");
            btnWallEraser.OnPress += OnBtnWallEraser;
            btnFloorEraser = new GUI_SButton(Global.Game, new Rectangle(btnFloor.Bounds.Right + 10, btnFloor.Bounds.Y, 150, btnFloor.Bounds.Height), Color.PowderBlue, "Floor Eraser");
            btnFloorEraser.OnPress += OnBtnFloorEraser;

            panel = new GUI_Panel();
            panel.AddItem(btnWall);
            panel.AddItem(btnFloor);
            panel.AddItem(btnWallEraser);
            panel.AddItem(btnFloorEraser);
            panel.Enabled = true;

            State = state.Idle;
        }

        #region EventHandlers
        private void OnBtnWall(object o, EventArgs e)
        {
            if (Brush != null) Brush.Destroy();
            Brush = new GameObject();
            Brush.AddComponent(new CPhysRecSensor(TileManager.tileWidth, TileManager.tileHeight, new Vector2(Input.X + RenderManager.camera.Position.X, Input.Y + RenderManager.camera.Position.Y), Category.Cat2, Category.Cat1 | Category.Cat3 | Category.Cat4));
            Brush.AddComponent(new CRenderTile(RenderLayers.Wall, 3, false));
            Brush.AddComponent(new CTileGroup());
            Brush.AddComponent(new COccluderRect(new Vector2(TileManager.tileWidth, TileManager.tileHeight), Brush.X, Brush.Y));
            Brush.GetComponent<CTileGroup>().Group = tileGroup;
            SetTileBrushLoc();
            State = state.Wall;
        }

        private void OnBtnFloor(object o, EventArgs e)
        {
            if (Brush != null) Brush.Destroy();
            Brush = new GameObject();
            Brush.AddComponent(new CRenderTile(RenderLayers.Floor, 2, false));
            Brush.AddComponent(new CTileGroup());
            Brush.GetComponent<CTileGroup>().Group = tileGroup;
            SetTileBrushLoc();
            State = state.Floor;
        }

        private void OnBtnWallEraser(object o, EventArgs e)
        {
            if (Brush != null) Brush.Destroy();
            Brush = null;
            State = state.WallErase;
        }

        private void OnBtnFloorEraser(object o, EventArgs e)
        {
            if (Brush != null) Brush.Destroy();
            Brush = null;
            State = state.FloorErase;
        }

        private void OnTxtTileNumChange(object o, EventArgs e)
        {
            //Check if the value is valid
            int newVal;
            int.TryParse(txtTileNum.Text, out newVal);

            if (newVal > 0 && newVal < 7)
            {
                //Change the tile group of the current brush.
                CTileGroup temp = Brush.GetComponent<CTileGroup>();
                temp.Group = newVal;
                tileGroup = newVal;
            }
        }
        #endregion

        public void Update(GameTime GT)
        {
            panel.Update(GT); //Update buttons

            switch (State)
            {
                case state.Wall:
                    SetTileBrushLoc();
                    txtTileNum.Update(GT);

                    if (Input.IsDown(MouseButtons.Left) && !Input.IsMouseOrGamepadHandled)
                    {
                        block.AddWall(Brush);
                        Brush = null;
                        OnBtnWall(null, null);
                    }
                    break;
                case state.WallErase:
                    if (Input.IsDown(MouseButtons.Left))
                        block.RemoveWallTile(Input.X + RenderManager.camera.Position.X, Input.Y + RenderManager.camera.Position.Y);
                    break;
                case state.Floor:
                    SetTileBrushLoc();
                    txtTileNum.Update(GT);

                    if (Input.IsDown(MouseButtons.Left) && !Input.IsMouseOrGamepadHandled)
                    {
                        block.AddFloor(Brush);
                        Brush = null;
                        OnBtnFloor(null, null);
                    }
                    break;
                case state.FloorErase:
                    if (Input.IsDown(MouseButtons.Left))
                        block.RemoveFloorTile(Input.X + RenderManager.camera.Position.X, Input.Y + RenderManager.camera.Position.Y);
                    break;
            }
        }

        public void Draw(SpriteBatch SB)
        {
            panel.Draw(SB); //Draw buttons

            if (State == state.Floor || State == state.Wall)
            {
                txtTileNum.Draw(SB);
            }
        }

        public void Close()
        {
            txtTileNum.Text = tileGroup.ToString();
            panel.Enabled = false; //Disable buttons
            Brush.Destroy();
            Brush = null;
        }

        public void Open()
        {
            panel.Enabled = true;
            switch (State)
            {
                case state.Floor:
                    OnBtnFloor(null, null);
                    break;
                case state.Wall:
                    OnBtnWall(null, null);
                    break;
            }
        }

        private void SetTileBrushLoc()
        {
            //Updates the brush's location, locking it to valid tile positions
            //Brush.X = ((int)((MathHelper.Clamp((Input.X + RenderManager.camera.Position.X), 0f, block.Dimension) / TileManager.tileWidth)) * TileManager.tileWidth + TileManager.tileWidth / 2;
            //Brush.Y = ((int)((Input.Y + RenderManager.camera.Position.Y) / TileManager.tileHeight)) * TileManager.tileHeight + TileManager.tileHeight / 2;
            //Brush.X = ((int)(MathHelper.Clamp((Input.X + RenderManager.camera.Position.X), 0f, block.Dimension) / TileManager.tileWidth)) * TileManager.tileWidth + TileManager.tileWidth / 2;

            Brush.X = MathHelper.Clamp(((int)((Input.X + RenderManager.camera.Position.X) / TileManager.tileWidth)), 0f, block.Dimension - 1) * TileManager.tileWidth + TileManager.tileWidth / 2;
            Brush.Y = MathHelper.Clamp(((int)((Input.Y + RenderManager.camera.Position.Y) / TileManager.tileHeight)), 0f, block.Dimension - 1) * TileManager.tileHeight + TileManager.tileHeight / 2;
        }
    }
}
