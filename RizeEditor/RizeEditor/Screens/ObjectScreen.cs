﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RizeEditor.Utilities.GUI;
using RizeEditor.Panes;
using RizeEditor.Components;
using RizeEditor.Components.Lighting;
using RizeEditor.Utilities.UserInput;
using RizeEditor.Factories;
using RizeEditor.Managers;
using FarseerPhysics;
using FarseerPhysics.Dynamics;

namespace RizeEditor.Screens
{
    class ObjectScreen : Screen
    {
        private GUI_SButton btnNew;
        private GUI_SButton btnSelect;
        private GUI_Panel panel;
        private GUI_STextBox txtID;
        private GUI_CMenu<EditPane> cMenu;

        private SemiBlock block;
        public SemiBlock Block { get { return block; } set { block = value; } }
        private GameObject Brush;
        private int BrushID;

        private GUI_CMenu<EditPane> contextMenu;
        private EditPane editPane;

        enum state
        {
            Idle,
            New,
            Selecting,
            Selected,
            Editing
        }

        private state State;

        public ObjectScreen(SemiBlock block)
        {
            this.block = block;
            contextMenu = new GUI_CMenu<EditPane>(Global.Game, Vector2.Zero, Color.Gray);
            contextMenu.OnSelected += OnMenuSelect;
            contextMenu.Enabled = false;

            BrushID = 101;

            btnNew = new GUI_SButton(Global.Game, new Rectangle(20, 150, 100, 35), Color.PowderBlue, "New");
            btnNew.OnPress += OnNew;
            btnSelect = new GUI_SButton(Global.Game, new Rectangle(btnNew.Bounds.X, btnNew.Bounds.Bottom + 10, btnNew.Bounds.Width, btnNew.Bounds.Height),
                Color.PowderBlue, "Select");
            btnSelect.OnPress += OnSelect;
            panel = new GUI_Panel();
            panel.AddItem(btnNew);
            panel.AddItem(btnSelect);
            panel.Enabled = true;

            txtID = new GUI_STextBox(Global.Game, new Rectangle(Global.Game.Window.ClientBounds.Width / 2, Global.Game.Window.ClientBounds.Height - 200, 100, 35), Color.LightGreen);
            txtID.OnTextChange += OnIDChange;
        }

        #region EventHandlers
        private void OnNew(object o, EventArgs e)
        {
            ShutDownPane();
            if(Brush != null)Brush.Destroy();
            
            //We take the current valid brush ID and retrieve the placeholder for that ID.
            Brush = ObjectFactory.GetSensor(BrushID, new Vector2(Input.X + RenderManager.camera.Position.X, Input.Y + RenderManager.camera.Position.Y));

            State = state.New;
        }

        private void OnSelect(object o, EventArgs e)
        {
            ShutDownPane();
            if(Brush != null)Brush.Destroy();
            Brush = null;
            State = state.Selecting;
        }

        private void OnIDChange(object o, EventArgs e)
        {
            //allow a preview of the object if valid ID
            int newID;
            int.TryParse(txtID.Text, out newID);
            if (ObjectFactory.IsValidID(newID)) OnNew(null, null);
        }

        private void OnMenuSelect(EditPane p)
        {
            contextMenu.Clear();
            contextMenu.Enabled = false;
            State = state.Editing;
            editPane = p;
        }
        #endregion


        public void Update(GameTime GT)
        {

            switch (State)
            {
                case state.Idle:
                    panel.Update(GT);
                    break;
                case state.New:
                    panel.Update(GT);

                    if (Brush != null)
                    {
                        Brush.X = Input.X + RenderManager.camera.Position.X;
                        Brush.Y = Input.Y + RenderManager.camera.Position.Y;
                        if (Input.IsDown(Keys.OemPlus)) Brush.Rotation += 0.01f;
                        if (Input.IsDown(Keys.OemMinus)) Brush.Rotation -= 0.01f;

                        if (Brush.GetComponent<CPhysRecSensor>().IsColliding)
                        {
                            Brush.GetComponent<CRenderSprite>().Color = Color.Red;
                        }
                        else
                        {
                            Brush.GetComponent<CRenderSprite>().Color = Color.White;

                            if (Input.IsPressed(MouseButtons.Left) && !Input.IsMouseOrGamepadHandled)
                            {
                                //Place the object
                                GameObject newObj = ObjectFactory.GetObject(BrushID, Brush.Position, Brush.Rotation);
                                newObj.AddComponent(new CLightSource(Color.Gray, 350, Input.X + RenderManager.camera.Position.X, Input.Y + RenderManager.camera.Position.Y));
                                newObj.AddComponent(new CLightSource(Color.Blue, 500, Input.X + RenderManager.camera.Position.X, Input.Y + RenderManager.camera.Position.Y));
                                newObj.AddComponent(new CLightSource(Color.Red, 750, Input.X + RenderManager.camera.Position.X, Input.Y + RenderManager.camera.Position.Y));
                                newObj = null;
                                OnNew(null, null);
                            }
                        }
                    }

                    if (Input.IsPressed(Keys.Enter))
                    {
                        txtID.Typing = false;
                    }
                    break;
                case state.Selecting:
                    panel.Update(GT);

                    if (Input.IsPressed(MouseButtons.Left) && !Input.IsMouseOrGamepadHandled)
                    {
                        Fixture fix = PhysicsManager.PhysicsWorld.TestPoint(ConvertUnits.ToSimUnits(new Vector2(Input.X + RenderManager.camera.Position.X, Input.Y + RenderManager.camera.Position.Y)));
                        if (fix != null) //If the mouse clicked on an object
                        {
                            //Get the object clicked on and create a new context menu
                            GameObject obj = (fix.Body.UserData as Component).Owner;
                            contextMenu.Position = new Vector2(Input.X, Input.Y);
                            contextMenu.Enabled = true;

                            bool hasSome = false;
                            foreach (Component c in obj.GetComponents()) //Get all the components that are available to edit and fill the menu
                            {
                                hasSome = true;
                                Tuple<string, EditPane> p = c.GetPane();
                                if (p != null) contextMenu.AddEntry(p.Item1, p.Item2);
                            }

                            if (hasSome)
                            {
                                State = state.Selected;
                                Input.IsMouseOrGamepadHandled = true;
                            }                            
                        }
                    }
                    break;
                case state.Selected:
                    contextMenu.Update(GT);

                    //Check for mouse presses on anything besides the context menu.
                    if (Input.IsPressed(MouseButtons.Left) && !Input.IsMouseOrGamepadHandled)
                    {
                        State = state.Selecting;
                        contextMenu.Clear();
                        contextMenu.Enabled = false;
                    }

                    break;
                case state.Editing:
                    //Only update the pane.
                    editPane.Update(GT);
                    panel.Update(GT);
                    break;
            }
            Console.WriteLine(State.ToString());
        }

        public void Draw(SpriteBatch SB)
        {
            panel.Draw(SB);
            contextMenu.Draw(SB);
            if (editPane != null) editPane.Draw(SB);
        }

        public void Close()
        {
            panel.Enabled = false;
            ShutDownPane();
            contextMenu.Clear();
            contextMenu.Enabled = false;
            if(Brush != null)Brush.Destroy();
            Brush = null;
            State = state.Idle;
        }

        public void Open()
        {
            panel.Enabled = true;
        }

        private void ShutDownPane()
        {
            if (editPane != null)
            {
                editPane.Destroy();
                editPane = null;
            }
        }
    }
}
