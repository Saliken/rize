﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RizeEditor.Screens
{
    interface Screen
    {
        void Update(GameTime GT);
        void Draw(SpriteBatch SB);
        void Close();
        void Open();
    }
}
