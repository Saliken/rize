﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RizeEditor.Utilities.GUI;
using RizeEditor.Utilities.UserInput;
using Microsoft.Xna.Framework.Input;

namespace RizeEditor.Screens
{
    class SavingScreen:Screen
    {
        private GUI_STextBox SavePrompt;
        private GUI_SButton SaveOK;
        private SemiBlock block;
        public SemiBlock Block { get { return block; } set { block = value; } }
        
        public SavingScreen(SemiBlock block)
            :base()
        {
            this.block = block;
            SavePrompt = new GUI_STextBox(Global.Game, new Rectangle(Global.Game.GraphicsDevice.Viewport.TitleSafeArea.Center.X - 175, Global.Game.GraphicsDevice.Viewport.TitleSafeArea.Center.Y - 25, 200, 50), Color.Gray);
            SavePrompt.Enabled = false;
            SaveOK = new GUI_SButton(Global.Game, new Rectangle(SavePrompt.Bounds.Right + 15, SavePrompt.Bounds.Y, 75, 50), Color.DarkGreen, "Save");
            SaveOK.OnPress += OnOK;
            SaveOK.Enabled = false;
        }

        private void OnOK(object o, EventArgs e)
        {
            //We need to send the block to the thing to be saved, with the new name.
            block.Name = SavePrompt.Text;
            Global.BlockSaver.SaveBlock(block);
        }

        public void Update(GameTime GT)
        {
            SavePrompt.Update(GT);
            SaveOK.Update(GT);
        }

        public void Draw(SpriteBatch SB)
        {
            SavePrompt.Draw(SB);
            SaveOK.Draw(SB);
        }

        public void Close()
        {
            SavePrompt.Enabled = false;
            SaveOK.Enabled = false;
        }

        public void Open()
        {
            SavePrompt.Enabled = true;
            SaveOK.Enabled = true;
            SavePrompt.Text = block.Name;
        }
    }
}
