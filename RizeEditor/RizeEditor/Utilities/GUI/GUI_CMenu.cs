﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RizeEditor.Managers;
using RizeEditor.Utilities.UserInput;

namespace RizeEditor.Utilities.GUI
{
    class GUI_CMenu<T>:GUI_SItemVector
    {

        private List<Tuple<string, T>> entries;
        private Texture2D backTex;
        private SpriteFont font;
        private Vector2 fontHeight;
        private Rectangle area;
        public override Vector2 Position
        {
            get
            {
                return base.Position;
            }
            set
            {
                base.Position = value;
                CalcArea();
            }
        }

        public delegate void ContextSelectHandler(T val);
        public event ContextSelectHandler OnSelected;

        private const float minWidth = 100;
        private const float minHeight = 25;

        public GUI_CMenu(Game g, Vector2 position, Color background)
            : base(g, position)
        {
            entries = new List<Tuple<string, T>>();
            backTex = RenderManager.CreateTex(g, 25, 25, background);
            font = g.Content.Load<SpriteFont>("Fonts/Generic/Gen12");
            fontHeight = font.MeasureString("M");
            fontHeight.X = 0;
            area = Rectangle.Empty;
            CalcArea();
        }

        private void CalcArea()
        {
            //The height is the number of entries times height
            int n = entries.Count;
            float h = font.MeasureString("M").Y;
            area.Height = (int)Math.Max((h * n), minHeight);

            //The width is equal to the longest string
            float stLength = minWidth;
            foreach (Tuple<string, T> o in entries)
            {
                if (font.MeasureString(o.Item1).X > stLength)
                {
                    stLength = font.MeasureString(o.Item1).X;
                }
            }
            area.Width = (int)stLength;
            area.X = (int)Position.X;
            area.Y = (int)Position.Y;
        }

        public void AddEntry(string tag, T entry)
        {
            entries.Add(new Tuple<string, T>(tag, entry));
            CalcArea();
        }

        public void Clear()
        {
            entries.Clear();
            CalcArea();
        }

        protected override void UpdateExt(GameTime GT)
        {
            //Check for mouse position/clicks and fire events
            if (Input.IsPressed(MouseButtons.Left) && area.Contains(Input.X, Input.Y) && entries.Count != 0 && !Input.IsMouseOrGamepadHandled)
            {
                //no need to check X, just how far the Y value is down the list.
                int numSelected = (int)((Input.Y - Position.Y) / font.MeasureString("M").Y);
                Input.IsMouseOrGamepadHandled = true;
                OnSelected.Invoke(entries[numSelected].Item2);
            }
            base.UpdateExt(GT);
        }

        protected override void DrawExt(SpriteBatch SB)
        {
            //Draw the background
            SB.Draw(backTex, area, Color.White);

            //Draw each entries text starting from the top            
            int t = 0;
            foreach (Tuple<string, T> e in entries)
            {
                SB.DrawString(font, e.Item1, Position + (fontHeight * t), Color.Black);
                t++;
            }
            base.DrawExt(SB);
        }
    }
}
