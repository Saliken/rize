﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RizeEditor.Utilities.GUI
{
    class GUI_Panel
    {
        private List<GUI_SItem> items;
        private bool enabled;
        public bool Enabled { get { return enabled; } set { enabled = value; SetEnabled(); } }

        public GUI_Panel()
        {
            items = new List<GUI_SItem>();
            enabled = true;
        }

        private void SetEnabled()
        {
            foreach (GUI_SItem i in items)
            {
                i.Enabled = enabled;
            }
        }

        public void AddItem(GUI_SItem item)
        {
            items.Add(item);
        }

        public void Update(GameTime gt)
        {
            foreach (GUI_SItem i in items)
            {
                i.Update(gt);
            }
        }

        public void Draw(SpriteBatch sb)
        {
            foreach (GUI_SItem i in items)
            {
                i.Draw(sb);
            }
        }
    }
}
