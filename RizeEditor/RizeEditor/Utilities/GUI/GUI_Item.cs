﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace RizeEditor.Utilities.GUI
{
    public enum Justification
    {
        Left,
        Right,
        Center,
        TopLeft,
        TopRight,
        TopCenter,
        BottomLeft,
        BottomRight,
        BottomCenter
    }

    abstract class GUI_Item
    {
        private Rectangle bounds;
        public Rectangle Bounds { get { return bounds; } set { bounds = value; OnBoundsChange(); } }
        public bool Enabled { get; set; }

        public GUI_Item(int X, int Y, int Width, int Height)
        {
            Bounds = new Rectangle(X, Y, Width, Height);
            this.Enabled = true;
        }
        public GUI_Item(int X, int Y, int Width, int Height, bool Enabled)
        {
            Bounds = new Rectangle(X, Y, Width, Height);
            this.Enabled = Enabled;
        }

        public void Update(GameTime gameTime)
        {
            if (Enabled) UpdateExt(gameTime);
        }
        public void Draw(SpriteBatch SB)
        {
            Rectangle current = SB.GraphicsDevice.ScissorRectangle;
            SB.GraphicsDevice.ScissorRectangle = bounds;
            if(Enabled) DrawExt(SB);
            SB.GraphicsDevice.ScissorRectangle = current;
        }

        protected virtual void DrawExt(SpriteBatch SB) { }
        protected virtual void UpdateExt(GameTime GT) { }
        protected virtual void OnBoundsChange() { }
        protected virtual void OnDisable() { }
        protected virtual void OnEnable() { }
    }
}
