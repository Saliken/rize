﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RizeEditor.Utilities.GUI
{
    class GUI_SItemVector:GUI_SItem
    {
        protected Vector2 position;
        public virtual Vector2 Position { get { return position; } set { position = value; } }

        public GUI_SItemVector(Game game, Vector2 Position)
            : base(game)
        {
            this.position = Position;
        }

        protected virtual void OnPositionChange() { }
    }
}
