﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RizeEditor.Utilities.UserInput;
using RizeEditor.Managers;

namespace RizeEditor.Utilities.GUI
{
    class GUI_Button:GUI_Item
    {
        //For the text
        private string text;
        public string Text { get { return text; } set { text = value; TextJust.CreateVector(Font, value, out TextOrigin); } }
        public Color Text_Color { get; set; }
        private SpriteFont font;
        public SpriteFont Font { get { return font; } set { font = value; TextJust.CreateVector(value, text, out TextOrigin); } }
        private Texture2D background;
        public Texture2D Background { get { return background; } set { background = value; ButtonJust.CreateVector(value, out Origin); } }
        private Justification textJust;
        public Justification TextJust { get { return textJust; } set { textJust = value; value.CreateVector(font, text, out TextOrigin); UpdateTextPos(); } }
        private Justification buttonJust;
        public Justification ButtonJust { get { return buttonJust; } set { buttonJust = value; value.CreateVector(background, out Origin); } }
        private Vector2 Origin;
        private Vector2 TextOrigin;
        private Vector2 TextPosition;

        public event EventHandler OnPress;


        public GUI_Button(int X, int Y, int Width, int Height, Justification? TextJust, Justification? ButtonJust, Texture2D Background)
            :base(X, Y, Width, Height)
        {   
            this.Text = "";
            this.Text_Color = Color.Black;
            this.Background = Background;
            if (TextJust.HasValue) TextJust = TextJust.Value;
            else TextJust = Justification.TopLeft;
            if (ButtonJust.HasValue) this.ButtonJust = ButtonJust.Value;
            else this.ButtonJust = Justification.TopLeft;
            this.TextJust.CreateVector(Background, out Origin);
            this.TextJust.CreateVector(Font, Text, out TextOrigin);
            UpdateTextPos();
        }

        public GUI_Button(int X, int Y, int Width, int Height, Justification? TextJust, Justification? ButtonJust, Texture2D Background, string Text, Color TextColor)
            :base(X, Y, Width, Height)
        {
            this.Text = Text;
            this.Text_Color = TextColor;
            this.Background = Background;
            if (TextJust.HasValue) TextJust = TextJust.Value;
            else TextJust = GUI.Justification.TopLeft;
            if (ButtonJust.HasValue) this.ButtonJust = ButtonJust.Value;
            else this.ButtonJust = Justification.TopLeft;
            this.TextJust.CreateVector(Background, out Origin);
            this.TextJust.CreateVector(Font, Text, out TextOrigin);
            UpdateTextPos();
        }

        protected override void OnBoundsChange()
        {
            UpdateTextPos();
            base.OnBoundsChange();
        }

        private void UpdateTextPos()
        {
            switch (TextJust)
            {
                case Justification.Left:
                    TextPosition.X = Bounds.Left;
                    TextPosition.Y = Bounds.Height / 2;
                    break;
                case Justification.Right:
                    TextPosition.X = Bounds.Right;
                    TextPosition.Y = Bounds.Height / 2;
                    break;
                case Justification.Center:
                    TextPosition.X = Bounds.Width / 2;
                    TextPosition.Y = Bounds.Height / 2;
                    break;
                case Justification.TopLeft:
                    TextPosition.X = Bounds.Left;
                    TextPosition.Y = Bounds.Top;
                    break;
                case Justification.TopRight:
                    TextPosition.X = Bounds.Right;
                    TextPosition.Y = Bounds.Top;
                    break;
                case Justification.TopCenter:
                    TextPosition.X = Bounds.Width / 2;
                    TextPosition.Y = Bounds.Top;
                    break;
                case Justification.BottomLeft:
                    TextPosition.X = Bounds.Left;
                    TextPosition.Y = Bounds.Bottom;
                    break;
                case Justification.BottomRight:
                    TextPosition.X = Bounds.Right;
                    TextPosition.Y = Bounds.Bottom;
                    break;
                case Justification.BottomCenter:
                    TextPosition.X = Bounds.Width / 2;
                    TextPosition.Y = Bounds.Bottom;
                    break;
                default:
                    break;
            }
        }

        protected override void UpdateExt(GameTime GT)
        {
            if (Bounds.Contains(Input.X + (int)RenderManager.camera.Position.X, Input.Y + (int)RenderManager.camera.Position.Y))
            {
                if (Input.IsReleased(MouseButtons.Left))
                {
                    OnPress.Invoke(this, EventArgs.Empty);
                }
            }
            base.UpdateExt(GT);
        }

        protected override void DrawExt(SpriteBatch SB)
        {
            SB.Draw(Background, Bounds, null, Color.White, 0f, Origin, SpriteEffects.None, 1f);
            SB.DrawString(Font, Text, TextPosition, Text_Color, 0f, TextOrigin, 1f, SpriteEffects.None, 1f);
            base.DrawExt(SB);
        }
    }
}
