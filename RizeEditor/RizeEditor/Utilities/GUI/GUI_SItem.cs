﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RizeEditor.Utilities.GUI
{
    public enum Orient
    {
        Left,
        Center,
        Right
    }

    class GUI_SItem
    {
        private bool enabled;
        public bool Enabled { get { return enabled; } set { if (value != enabled) { enabled = value; if (value) { OnEnable(); } else { OnDisable(); } } } }
        protected Game game;
        protected Rectangle scissorRect;

        protected static int minFontSize = 12;
        protected static int maxFontSize = 20;

        public GUI_SItem(Game game)
        {
            this.game = game;
            this.Enabled = true;
        }

        public void Update(GameTime gameTime)
        {
            if (Enabled) UpdateExt(gameTime);
        }

        public void Draw(SpriteBatch SB)
        {
            if (Enabled)
            {
                if (scissorRect.Width != 0 && scissorRect.Height != 0)
                {
                    Rectangle current = SB.GraphicsDevice.ScissorRectangle;
                    SB.GraphicsDevice.ScissorRectangle = scissorRect;
                    DrawExt(SB);
                    SB.GraphicsDevice.ScissorRectangle = current;
                }
                else DrawExt(SB);
            }
        }

        protected virtual void DrawExt(SpriteBatch SB) { }
        protected virtual void UpdateExt(GameTime GT) { }
        
        protected virtual void OnDisable() { }
        protected virtual void OnEnable() { }
        public virtual void Reset() { }
    }
}
