﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace RizeEditor.Utilities.GUI
{
    class GUI_Label:GUI_Item
    {
        private string text;
        public string Text { get { return text; } set { text = value; just.CreateVector(font, value, out TextOrigin); } }
        public Color Text_Color { get; set; }
        private SpriteFont font;
        public SpriteFont Font { get { return font; } set { font = value; Just.CreateVector(value, text, out TextOrigin); } }
        private Texture2D background;
        public Texture2D Background { get { return background; } set { background = value; just.CreateVector(value, out Origin); } }
        private Justification just;
        public Justification Just { get { return just; } set { just = value; value.CreateVector(background, out Origin); value.CreateVector(font, text, out TextOrigin); } }
        private Vector2 Origin;
        private Vector2 TextOrigin;

        public GUI_Label(int X, int Y, int Width, int Height, Justification? Justification, Texture2D Background, SpriteFont Font)
            :base(X, Y, Width, Height)
        {            
            text = "";
            this.Text_Color = Color.White;
            background = Background;
            font = Font;

            if (Justification.HasValue) Just = Justification.Value;
            else Just = GUI.Justification.TopLeft;            
            Just.CreateVector(Background, out Origin);
            Just.CreateVector(Font, Text, out TextOrigin);
        }

        public GUI_Label(int X, int Y, int Width, int Height, Justification? Justification, Texture2D Background, SpriteFont Font, string Text, Color TextColor)
            :base(X, Y, Width, Height)
        {
            text = Text;
            this.Text_Color = TextColor;
            background = Background;
            font = Font;

            if (Justification.HasValue) Just = Justification.Value;
            else Just = GUI.Justification.TopLeft;
            Just.CreateVector(Background, out Origin);
            Just.CreateVector(Font, Text, out TextOrigin);
        }

        protected override void UpdateExt(GameTime GT)
        {
            base.UpdateExt(GT);
        }

        protected override void DrawExt(SpriteBatch SB)
        {
            SB.Draw(Background, Bounds, null, Color.White, 0f, Origin, SpriteEffects.None, 1f);
            SB.DrawString(Font, Text, new Vector2(Bounds.X, Bounds.Y), Text_Color, 0f, TextOrigin, 1f, SpriteEffects.None, 1f);
            base.DrawExt(SB);
        }

        
    }
}
