﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RizeEditor.Utilities.UserInput;

namespace RizeEditor.Utilities.GUI
{
    class GUI_STextBox:GUI_SItemBounds
    {
        public SpriteFont Font { get; set; }
        public string Text { get; set; }
        private Vector2 TextPos;
        public Texture2D Background { get; set; }
        public bool Typing { get; set; }

        //Cursor
        private bool Cursor;
        private int CursorTime;

        public event EventHandler OnTextChange;

        public GUI_STextBox(Game game, Rectangle bounds, Texture2D background)
            : base(game, bounds)
        {
            Background = background;
            Text = "";
            SetBestFont();
            Cursor = false;
            CursorTime = 0;
            Typing = false;
            TextPos = new Vector2(Bounds.X, Bounds.Y);
        }

        public GUI_STextBox(Game game, Rectangle bounds, Color background)
            :base(game, bounds)
        {
            Background = RizeEditor.Managers.RenderManager.CreateTex(game, bounds.Width, bounds.Height, background);
            Text = "";
            SetBestFont();
            Cursor = false;
            CursorTime = 0;
            Typing = false;
            TextPos = new Vector2(Bounds.X, Bounds.Y);
        }

        protected override void UpdateExt(GameTime GT)
        {
            CursorTime += (int)GT.ElapsedGameTime.TotalMilliseconds;
            if (CursorTime >= 500)
            {
                Cursor = (!Cursor);
                CursorTime = 0;
            }

            if (Input.IsReleased(MouseButtons.Left))
            {
                if(bounds.Contains(Input.X, Input.Y) && Input.IsMouseOrGamepadHandled == false)
                {
                    Typing = true;
                    Cursor = true;
                    Input.IsMouseOrGamepadHandled = true;
                }
                else
                {
                    Typing = false;
                }
            }

            if (bounds.Contains(Input.X, Input.Y)) Input.IsMouseOrGamepadHandled = true;

            if (Typing)
            {

                CursorTime += (int)GT.ElapsedGameTime.TotalMilliseconds;
                if (CursorTime >= 500)
                {
                    Cursor = (!Cursor);
                    CursorTime = 0;
                }

                string curText = Text;
                Text += Input.Text;
                if (Input.BackSpace) Text = Text.Substring(0, Math.Max(0, Text.Length - 1));
                if (Text != curText && OnTextChange != null) OnTextChange.Invoke(this, EventArgs.Empty);
            }
            else { Cursor = false; CursorTime = 0; }
            
            //Make sure the newest text is on the screen
            TextPos.X = Bounds.X - Math.Max((Font.MeasureString(Text).X - (Bounds.Width - 20)), 0);
            
            base.UpdateExt(GT);
        }

        public override void Reset()
        {
            Typing = false;
            base.Reset();
        }

        protected override void OnDisable()
        {
            Reset();
            base.OnDisable();
        }

        protected override void DrawExt(SpriteBatch SB)
        {
            SB.Draw(Background, Bounds, Color.White);
            if (Cursor) { SB.DrawString(Font, (Text + "|"), TextPos, Color.Black); }
            else SB.DrawString(Font, Text, TextPos, Color.Black);
            base.DrawExt(SB);
        }

        private void SetBestFont()
        {
            int curSizeI = maxFontSize;
            string curSize = curSizeI.ToString();
            while (true)
            {
                SpriteFont tempf = game.Content.Load<SpriteFont>("Fonts/Generic/Gen" + curSize);
                if (tempf.MeasureString("M").Y > Bounds.Height)
                {
                    curSizeI -= 2;
                    curSize = curSizeI.ToString();
                    if (curSizeI < minFontSize) break;
                }
                else
                {
                    Font = tempf;
                    break;
                }
            }
        }
    }
}
