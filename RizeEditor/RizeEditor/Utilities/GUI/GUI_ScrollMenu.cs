﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RizeEditor.Managers;
using RizeEditor.Utilities.UserInput;

namespace RizeEditor.Utilities.GUI
{
    class GUI_ScrollMenu<T>:GUI_SItemBounds
    {
        private List<Tuple<string, T>> entries;
        private Texture2D backTex;
        private SpriteFont font;
        private Vector2 fontHeight;

        private float curScroll;
        private float maxScroll;

        public delegate void MenuSelectHandler(T val);
        public event MenuSelectHandler OnSelected;

        public GUI_ScrollMenu(Game game, Rectangle bounds, Color backColor)
            :base(game, bounds)
        {
            backTex = RenderManager.CreateTex(game, 1, 1, backColor);
            entries = new List<Tuple<string, T>>();
            font = game.Content.Load<SpriteFont>("Fonts/Generic/Gen12");
            fontHeight = new Vector2(0, font.MeasureString("M").Y);
            curScroll = 0;
            maxScroll = 0;
        }

        public void Clear()
        {
            entries.Clear();
            CalcMaxScroll();
        }

        public void AddEntry(string name, T entry)
        {
            entries.Add(new Tuple<string, T>(name, entry));
            CalcMaxScroll();
        }

        private void CalcMaxScroll()
        {
            maxScroll = MathHelper.Max(((entries.Count * fontHeight.Y) - bounds.Height), 0f);
        }

        protected override void UpdateExt(GameTime GT)
        {
            if (bounds.Contains(Input.X, Input.Y))
            {
                if (Input.MouseScrolledUp)
                {
                    curScroll = Math.Max(0f, curScroll - 5);
                }

                if (Input.MouseScrolledDown)
                {
                    curScroll = Math.Min(maxScroll, curScroll + 5);
                }

                if (Input.IsPressed(MouseButtons.Left) && !Input.IsMouseOrGamepadHandled)
                {
                    //Find the value by normalizing the list and moving the mouse with it
                    int entry = (int)(((Input.Y - bounds.Y) + curScroll) / fontHeight.Y);
                    if(OnSelected != null) OnSelected.Invoke(entries[entry].Item2);
                    Input.IsMouseOrGamepadHandled = true;
                }
            }

            base.UpdateExt(GT);
        }

        protected override void DrawExt(SpriteBatch SB)
        {
            SB.Draw(backTex, bounds, Color.White);

            int i = 0;
            foreach (Tuple<string, T> entry in entries)
            {
                SB.DrawString(font, entry.Item1, new Vector2(bounds.X, bounds.Y - curScroll) + (fontHeight * i), Color.Black);
                i++;
            }
            base.DrawExt(SB);
        }
    }
}
