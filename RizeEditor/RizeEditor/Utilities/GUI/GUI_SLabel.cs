﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace RizeEditor.Utilities.GUI
{
    class GUI_SLabel:GUI_SItemVector
    {
        private string text;
        public string Text { get { return text; } set { text = value; UpdateOrient(); } }
        private SpriteFont font;
        public SpriteFont Font { get { return font; } set { font = value; UpdateOrient(); } }
        private Orient origin;
        public Orient Origin { get { return origin; } set { origin = value; UpdateOrient(); } }
        private Vector2 originLoc;
        private Color color;
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }
        public override Vector2 Position
        {
            get
            {
                return base.Position + originLoc;
            }
            set
            {
                base.Position = value;
            }
        }

        public GUI_SLabel(Game game, Vector2 position, string Text, Orient origin)
            : this(game, position, Text, origin, Color.Black) { }

        public GUI_SLabel(Game game, Vector2 position, string Text, Orient origin, Color color)
            : this(game, position, Text, origin, game.Content.Load<SpriteFont>("Fonts/Generic/Gen12"), color) { }

        public GUI_SLabel(Game game, Vector2 position, string Text, Orient origin, SpriteFont font)
            : this(game, position, Text, origin, font, Color.Black) { }

        public GUI_SLabel(Game game, Vector2 position, string Text, Orient origin, SpriteFont font, Color color)
            : base(game, position)
        {
            this.text = Text;
            this.origin = origin;
            this.originLoc = new Vector2();
            this.color = color;
            this.font = font;
            UpdateOrient();
        }

        private void UpdateOrient()
        {
            switch (origin)
            {
                case Orient.Left:
                    originLoc.X = 0;
                    originLoc.Y = 0;
                    break;
                case Orient.Center:
                    originLoc.X = font.MeasureString(text).X / 2;
                    originLoc.Y = 0;
                    break;
                case Orient.Right:
                    originLoc.X = font.MeasureString(text).X;
                    originLoc.Y = 0;
                    break;
                default:
                    break;
            }
        }

        protected override void DrawExt(SpriteBatch SB)
        {
            SB.DrawString(font, text, Position, color);
            base.DrawExt(SB);
        }        
    }
}
