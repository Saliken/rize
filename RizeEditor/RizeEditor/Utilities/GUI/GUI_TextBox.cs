﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RizeEditor.Utilities.GUI
{
    class GUI_TextBox:GUI_Item
    {
        public Justification Just { get; set; }
        public Texture2D Background { get; set; }
        public string Text { get; set; }
        public Color Text_Color { get; set; }
        public SpriteFont Font { get; set; }
        private Vector2 Origin;

        public GUI_TextBox(int X, int Y, int Width, int Height)
            :base(X, Y, Width, Height)
        {

        }

        public GUI_TextBox(int X, int Y, int Width, int Height, Justification? just, Texture2D Background, SpriteFont Font)
            :base(X, Y, Width, Height)
        {
            if (just.HasValue) Just = just.Value;
            else Just = Justification.TopLeft;

            this.Background = Background;
            this.Text = "";
            this.Text_Color = Color.Black;
            this.Font = Font;
        }

        public GUI_TextBox(int X, int Y, int Width, int Height, Justification? just, Texture2D Background, SpriteFont Font, string Text, Color TextColor)
            : base(X, Y, Width, Height)
        {
            if (just.HasValue) Just = just.Value;
            else Just = Justification.TopLeft;

            this.Background = Background;
            this.Text = Text;
            this.Font = Font;
            this.Text_Color = TextColor;
        }

        protected override void UpdateExt(GameTime GT)
        {
            base.UpdateExt(GT);
        }

        protected override void DrawExt(SpriteBatch SB)
        {
            SB.Draw(Background, Bounds, null, Color.White, 0f, Origin, SpriteEffects.None, 1f);
            SB.DrawString(Font, Text, new Vector2(Bounds.X, Bounds.Y), Text_Color, 0f, Origin, 1f, SpriteEffects.None, 1f);
            base.DrawExt(SB);
        }
    }
}
