﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RizeEditor.Utilities.GUI
{
    class GUI_SItemBounds:GUI_SItem
    {
        protected Rectangle bounds;
        public Rectangle Bounds { get { return bounds; } set { bounds = value; OnBoundsChange(); } }

        public GUI_SItemBounds(Game game, Rectangle Bounds)
            : base(game)
        {
            this.bounds = Bounds;
            scissorRect = bounds;
        }

        protected override void DrawExt(Microsoft.Xna.Framework.Graphics.SpriteBatch SB)
        {            
            base.DrawExt(SB);            
        }

        protected virtual void OnBoundsChange() { }
    }
}
