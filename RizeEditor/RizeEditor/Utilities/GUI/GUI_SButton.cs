﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RizeEditor.Utilities.UserInput;
using RizeEditor.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RizeEditor.Utilities.GUI
{
    class GUI_SButton:GUI_SItemBounds
    {
        private string text;
        public string Text { get { return text; } set { text = value; UpdateOrigin(); } }
        private Vector2 textOrigin;
        private SpriteFont font;
        private SpriteFont Font { get { return font; } set { font = value; UpdateOrigin(); } }
        private Texture2D texture;
        public Texture2D Texture { get { return texture; } set { texture = value; } }

        public event EventHandler OnPress;

        private const int PADDING = 15;

        public GUI_SButton(Game game, Rectangle bounds, Color backColor, string Text)
            :base(game, bounds)
        {
            text = Text;
            textOrigin = Vector2.Zero;
            texture = RenderManager.CreateTex(game, bounds.Width, bounds.Height, backColor);
            SetBestFont();
            UpdateOrigin();
        }

        public GUI_SButton(Game game, Rectangle bounds, Texture2D backTex)
            :base(game, bounds)
        {
            textOrigin = Vector2.Zero;
            texture = backTex;
            SetBestFont();
            UpdateOrigin();
        }

        private void UpdateOrigin()
        {
            textOrigin.X = font.MeasureString(text).X / 2;
            textOrigin.Y = font.MeasureString(text).Y / 2;
        }

        protected override void OnBoundsChange()
        {
            SetBestFont();
            UpdateOrigin();
            base.OnBoundsChange();
        }

        protected override void UpdateExt(GameTime GT)
        {
            if (bounds.Contains(Input.X, Input.Y))
            {
                
                if (Input.IsReleased(MouseButtons.Left) && !Input.IsMouseOrGamepadHandled)
                {
                    Input.IsMouseOrGamepadHandled = true;
                    if(OnPress != null) OnPress.Invoke(this, EventArgs.Empty);
                }

                Input.IsMouseOrGamepadHandled = true;
            }
            base.UpdateExt(GT);
        }

        protected override void DrawExt(SpriteBatch SB)
        {
            SB.Draw(texture, bounds, Color.White);
            SB.DrawString(font, text, new Vector2(bounds.Center.X, bounds.Center.Y), Color.Black, 0f, textOrigin, 1f, SpriteEffects.None, 1f);
            base.DrawExt(SB);
        }

        private void SetBestFont()
        {
            //Get the best fitting generic font
            int curSizeI = maxFontSize;
            string curSize = curSizeI.ToString();
            while (true)
            {
                SpriteFont tempf = game.Content.Load<SpriteFont>("Fonts/Generic/Gen" + curSize);
                if (tempf.MeasureString(text).X > (Bounds.Width - (PADDING * 2)))
                {
                    curSizeI -= 2;
                    curSize = curSizeI.ToString();
                    if (curSizeI < minFontSize)
                    {
                        font = tempf;
                        break;
                    }
                }
                else
                {
                    font = tempf;
                    break;
                }
            }
        }
    }
}
