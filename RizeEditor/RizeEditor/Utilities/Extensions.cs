﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RizeEditor.Utilities.GUI;
using RizeEditor.Managers;

namespace RizeEditor.Utilities
{
    static class Extensions
    {
        public static void CreateVector(this Justification just, SpriteFont Font, string Text, out Vector2 endVec)
        {
            Vector2 size = Font.MeasureString(Text);
            switch (just)
            {
                case GUI.Justification.Left:
                    endVec.X = 0;
                    endVec.Y = size.Y / 2;
                    break;
                case GUI.Justification.Right:
                    endVec.X = size.X;
                    endVec.Y = size.Y / 2;
                    break;
                case GUI.Justification.Center:
                    endVec.X = size.X / 2;
                    endVec.Y = size.Y / 2;
                    break;
                case GUI.Justification.TopLeft:
                    endVec.X = 0;
                    endVec.Y = 0;
                    break;
                case GUI.Justification.TopRight:
                    endVec.X = size.X;
                    endVec.Y = 0;
                    break;
                case GUI.Justification.TopCenter:
                    endVec.X = size.X / 2;
                    endVec.Y = 0;
                    break;
                case GUI.Justification.BottomLeft:
                    endVec.X = 0;
                    endVec.Y = size.Y;
                    break;
                case GUI.Justification.BottomRight:
                    endVec.X = size.X;
                    endVec.Y = size.Y;
                    break;
                case GUI.Justification.BottomCenter:
                    endVec.X = size.X / 2;
                    endVec.Y = size.Y;
                    break;
                default: endVec = Vector2.Zero;
                    break;
            }
        }
        public static void CreateVector(this Justification just, Texture2D texture, out Vector2 endVec)
        {
            switch (just)
            {
                case GUI.Justification.Left:
                    endVec.X = 0;
                    endVec.Y = texture.Bounds.Height / 2f;
                    break;
                case GUI.Justification.Right:
                    endVec.X = texture.Width;
                    endVec.Y = texture.Bounds.Height / 2f;
                    break;
                case GUI.Justification.Center:
                    endVec.X = texture.Bounds.Width / 2f;
                    endVec.Y = texture.Bounds.Height / 2f;
                    break;
                case GUI.Justification.TopLeft:
                    endVec.X = 0;
                    endVec.Y = 0;
                    break;
                case GUI.Justification.TopRight:
                    endVec.X = texture.Width;
                    endVec.Y = 0;
                    break;
                case GUI.Justification.TopCenter:
                    endVec.X = texture.Bounds.Width / 2f;
                    endVec.Y = 0;
                    break;
                case GUI.Justification.BottomLeft:
                    endVec.X = 0;
                    endVec.Y = texture.Height;
                    break;
                case GUI.Justification.BottomRight:
                    endVec.X = texture.Width;
                    endVec.Y = texture.Height;
                    break;
                case GUI.Justification.BottomCenter:
                    endVec.X = texture.Bounds.Width / 2f;
                    endVec.Y = texture.Height;
                    break;
                default: endVec = Vector2.Zero;
                    break;
            }
        }
        public static void CreateVector(this Justification just, Rectangle rec, out Vector2 endVec)
        {
            switch (just)
            {
                case Justification.Left:
                    endVec.X = 0;
                    endVec.Y = rec.Height / 2;
                    break;
                case Justification.Right:
                    endVec.X = rec.Width;
                    endVec.Y = rec.Height / 2;
                    break;
                case Justification.Center:
                    endVec.X = rec.Width / 2;
                    endVec.Y = rec.Height / 2;
                    break;
                case Justification.TopLeft:
                    endVec.X = 0;
                    endVec.Y = 0;
                    break;
                case Justification.TopRight:
                    endVec.X = rec.Width;
                    endVec.Y = 0;
                    break;
                case Justification.TopCenter:
                    endVec.X = rec.Width / 2;
                    endVec.Y = 0;
                    break;
                case Justification.BottomLeft:
                    endVec.X = 0;
                    endVec.Y = rec.Height;
                    break;
                case Justification.BottomRight:
                    endVec.X = rec.Width;
                    endVec.Y = rec.Height;
                    break;
                case Justification.BottomCenter:
                    endVec.X = rec.Width / 2;
                    endVec.Y = rec.Height;
                    break;
                default:
                    endVec = Vector2.Zero;
                    break;
            }
        }

        
    }
}
