﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace RizeEditor.Utilities.UserInput
{
    public abstract class InputAction
    {
        public bool IsPressed { get; protected set; }
        public bool IsReleased { get; protected set; }
        public bool IsDown { get; protected set; }
        public string Name { get; protected set; }

        internal void Refresh()
        {
            OnRefresh();
        }

        protected abstract void OnRefresh();
    }

    public class InputCommand : InputAction
    {
        public Keys? Key { get; set; }
        public MouseButtons? MouseButton { get; set; }
        public Buttons? Button { get; set; }
        public PlayerIndex Player { get; set; }

        public InputCommand(PlayerIndex Player, Keys? Key, MouseButtons? MouseButton, Buttons? Button)
            :base()
        {
            this.Player = Player;
            if (Key.HasValue) this.Key = Key;
            if (MouseButton.HasValue) this.MouseButton = MouseButton;
            if (Button.HasValue) this.Button = Button;
        }
        protected override void OnRefresh()
        {
            if(Key.HasValue)
            {
                IsDown = Input.IsDown(Key.Value);
                IsPressed = Input.IsPressed(Key.Value);
                IsReleased = Input.IsReleased(Key.Value);
            }
            else if (MouseButton.HasValue)
            {
                IsDown = Input.IsDown(MouseButton.Value);
                IsPressed = Input.IsPressed(MouseButton.Value);
                IsReleased = Input.IsReleased(MouseButton.Value);
            }
            else if (Button.HasValue)
            {
                IsDown = Input.IsDown(Player, Button.Value);
                IsPressed = Input.IsPressed(Player, Button.Value);
                IsReleased = Input.IsReleased(Player, Button.Value);
            }
        }
    }
}
