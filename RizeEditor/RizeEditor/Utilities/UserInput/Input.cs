﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RizeEditor.Utilities.UserInput
{
    public enum MouseButtons
    {
        Left,
        Middle,
        Right,
        XButton1,
        XButton2
    }

    class Input : GameComponent
    {
        //Keyboard
        private static KeyboardState _ksLast;
        private static KeyboardState _ksState;
        private static string _textInput;
        private static TimeSpan _BPStart;
        private static bool _backspace;

        //Mouse
        private static MouseState _mLast;
        private static MouseState _mState;

        //Virtual Mouse
        private static bool isVirtual;
        private static int mouseX;
        private static int mouseY;

        //GamePad
        private static GamePadState _gpALast;
        private static GamePadState _gpBLast;
        private static GamePadState _gpCLast;
        private static GamePadState _gpDLast;

        private static GamePadState _gpAState;
        private static GamePadState _gpBState;
        private static GamePadState _gpCState;
        private static GamePadState _gpDState;

        //Flags
        private static bool isMouseOrGamepadHandled;

        //Player Key Bindings
        private static Dictionary<string, InputAction> ActionsA;
        private static Dictionary<string, InputAction> ActionsB;
        private static Dictionary<string, InputAction> ActionsC;
        private static Dictionary<string, InputAction> ActionsD;

        private static bool Initialized = false;

        public Input(Game game, bool virtualMouse)
            : base(game)
        {
            if (!Initialized)
            {
                isVirtual = virtualMouse;
                game.Components.Add(this);
            }
        }

        public override void Initialize()
        {
            if (!Initialized)
            {

                //--Init Actions--
                ActionsA = new Dictionary<string, InputAction>();
                ActionsB = new Dictionary<string, InputAction>();
                ActionsC = new Dictionary<string, InputAction>();
                ActionsD = new Dictionary<string, InputAction>();

                //--Initialize states--
                //Keyboard
                _ksLast = Keyboard.GetState();
                _ksState = _ksLast;
                _textInput = "";
                _BPStart = TimeSpan.MinValue;

                //Virtual Mouse
                if (isVirtual)
                {
                    //~Set real mouse to the center of the game viewport (window)
                    Mouse.SetPosition(Game.GraphicsDevice.Viewport.Bounds.Center.X,
                        Game.GraphicsDevice.Viewport.Bounds.Center.Y);
                    //~Set Virtual mouse to the same
                    mouseX = Mouse.GetState().X;
                    mouseY = Mouse.GetState().Y;
                }

                //Mouse
                _mLast = Mouse.GetState();
                _mState = _mLast;

                //GamePads
                _gpALast = GamePad.GetState(PlayerIndex.One);
                _gpBLast = GamePad.GetState(PlayerIndex.Two);
                _gpCLast = GamePad.GetState(PlayerIndex.Three);
                _gpDLast = GamePad.GetState(PlayerIndex.Four);

                _gpAState = _gpALast;
                _gpBState = _gpBLast;
                _gpCState = _gpCLast;
                _gpDState = _gpCLast;

                Initialized = true;
            }

            base.Initialize();

        }

        public override void Update(GameTime gameTime)
        {
            //Reset Flag
            isMouseOrGamepadHandled = false;

            //--Update States
            //Keyboard
            _ksLast = _ksState;
            _ksState = Keyboard.GetState();
            _textInput = GetInputString();
            if (IsDown(Keys.Back))
            {
                if (IsPressed(Keys.Back))
                {
                    _backspace = true;
                    _BPStart = gameTime.TotalGameTime;
                }
                else
                {
                    if (gameTime.TotalGameTime.TotalMilliseconds - _BPStart.TotalMilliseconds >= 650)
                    {
                        _backspace = true;
                    }
                    else
                    {
                        _backspace = false;
                    }
                }
            }
            else _backspace = false;

            //Mouse
            _mLast = _mState;
            _mState = Mouse.GetState();

            //Virtual Mouse
            if (isVirtual)
            {
                mouseX += _mState.X - _mLast.X;
                mouseY += _mState.Y - _mLast.Y;
                mouseX = (int)MathHelper.Clamp(mouseX, Game.GraphicsDevice.Viewport.Bounds.Left, Game.GraphicsDevice.Viewport.Bounds.Right);
                mouseY = (int)MathHelper.Clamp(mouseX, Game.GraphicsDevice.Viewport.Bounds.Top, Game.GraphicsDevice.Viewport.Bounds.Bottom);
            }

            //GamePads
            _gpALast = _gpAState;
            _gpBLast = _gpBState;
            _gpCLast = _gpCState;
            _gpDLast = _gpDState;
            _gpAState = GamePad.GetState(PlayerIndex.One);
            _gpBState = GamePad.GetState(PlayerIndex.Two);
            _gpCState = GamePad.GetState(PlayerIndex.Three);
            _gpDState = GamePad.GetState(PlayerIndex.Four);

            //Reset Mouse Position (Capture)
            if (isVirtual)
            {
                Mouse.SetPosition(Game.GraphicsDevice.Viewport.Bounds.Center.X, Game.GraphicsDevice.Viewport.Bounds.Center.Y);
            }

            //Refresh Actions
            foreach (InputAction a in ActionsA.Values) { a.Refresh(); }
            foreach (InputAction b in ActionsB.Values) { b.Refresh(); }
            foreach (InputAction c in ActionsC.Values) { c.Refresh(); }
            foreach (InputAction d in ActionsD.Values) { d.Refresh(); }

            base.Update(gameTime);
        }

        private static string GetInputString()
        {
            string output = "";
            bool shift = (IsDown(Keys.LeftShift) || IsDown(Keys.RightShift));

            foreach (Keys key in _ksState.GetPressedKeys())
            {
                if (_ksLast.IsKeyDown(key))
                    continue;

                if (key >= Keys.A && key <= Keys.Z)
                    output += key.ToString();
                else if (key >= Keys.NumPad0 && key <= Keys.NumPad9)
                    output += ((int)(key - Keys.NumPad0)).ToString();
                else if (key >= Keys.D0 && key <= Keys.D9)
                {
                    string num = ((int)(key - Keys.D0)).ToString();

                    if (shift)
                    {
                        switch (num)
                        {
                            case "1":
                                num = "!";
                                break;
                            case "2":
                                num = "@";
                                break;
                            case "3":
                                num = "#";
                                break;
                            case "4":
                                num = "$";
                                break;
                            case "5":
                                num = "%";
                                break;
                            case "6":
                                num = "^";
                                break;
                            case "7":
                                num = "&";
                                break;
                            case "8":
                                num = "*";
                                break;
                            case "9":
                                num = "(";
                                break;
                            case "0":
                                num = ")";
                                break;
                            default:
                                break;
                        }
                    }
                    output += num;
                }

                else if (key == Keys.OemQuestion && shift)
                    output += "?";
                else if (key == Keys.OemPeriod && shift)
                    output += ">";
                else if (key == Keys.OemComma && shift)
                    output += "<";
                else if (key == Keys.OemSemicolon && shift)
                    output += ":";
                else if (key == Keys.OemQuotes && shift)
                    output += "\"";
                else if (key == Keys.OemOpenBrackets && shift)
                    output += "{";
                else if (key == Keys.OemCloseBrackets && shift)
                    output += "}";
                else if (key == Keys.OemPipe && shift)
                    output += "|";
                else if (key == Keys.OemMinus && shift)
                    output += "_";
                else if (key == Keys.OemPlus && shift)
                    output += "+";
                else if (key == Keys.OemTilde && shift)
                    output += "~";
                else if (key == Keys.OemPeriod)
                    output += ".";
                else if (key == Keys.OemQuotes)
                    output += "'";
                else if (key == Keys.Space)
                    output += " ";
                else if (key == Keys.OemMinus)
                    output += "-";
                else if (key == Keys.OemPlus)
                    output += "=";
                else if (key == Keys.OemComma)
                    output += ",";
                else if (key == Keys.OemQuestion)
                    output += "/";
                else if (key == Keys.OemSemicolon)
                    output += ";";
                else if (key == Keys.OemOpenBrackets)
                    output += "[";
                else if (key == Keys.OemCloseBrackets)
                    output += "]";
                else if (key == Keys.OemPipe)
                    output += "\\";
                else if (key == Keys.OemTilde)
                    output += "`";


                if (!shift)
                    output = output.ToLower();
            }
            return output;
        }

        #region Static Properties
        #region Keyboard
        public static bool IsDown(Keys key)
        {
            return _ksState.IsKeyDown(key);
        }
        public static bool IsPressed(Keys key)
        {
            return _ksLast.IsKeyUp(key) && _ksState.IsKeyDown(key);
        }
        public static bool IsReleased(Keys key)
        {
            return _ksLast.IsKeyDown(key) && _ksState.IsKeyUp(key);
        }
        public static string Text { get { return _textInput; } }
        public static bool BackSpace { get { return _backspace; } }
        #endregion
        #region Mouse
        public static int X
        {
            get
            {
                if (isVirtual)
                    return mouseX;
                else return _mState.X;
            }
        }
        public static int Y
        {
            get
            {
                if (isVirtual)
                    return mouseY;
                else return _mState.Y;
            }
        }
        public static bool IsPressed(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.Left:
                    return (_mLast.LeftButton == ButtonState.Released) && (_mState.LeftButton == ButtonState.Pressed);
                case MouseButtons.Right:
                    return (_mLast.RightButton == ButtonState.Released) && (_mState.RightButton == ButtonState.Pressed);
                case MouseButtons.Middle:
                    return (_mLast.MiddleButton == ButtonState.Released) && (_mState.MiddleButton == ButtonState.Pressed);
                case MouseButtons.XButton1:
                    return (_mLast.XButton1 == ButtonState.Released) && (_mState.XButton1 == ButtonState.Pressed);
                case MouseButtons.XButton2:
                    return (_mLast.XButton2 == ButtonState.Released) && (_mState.XButton2 == ButtonState.Pressed);
                default:
                    return false;

            }
        }
        public static bool IsReleased(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.Left:
                    return (_mLast.LeftButton == ButtonState.Pressed) && (_mState.LeftButton == ButtonState.Released);
                case MouseButtons.Right:
                    return (_mLast.RightButton == ButtonState.Pressed) && (_mState.RightButton == ButtonState.Released);
                case MouseButtons.Middle:
                    return (_mLast.MiddleButton == ButtonState.Pressed) && (_mState.MiddleButton == ButtonState.Released);
                case MouseButtons.XButton1:
                    return (_mLast.XButton1 == ButtonState.Pressed) && (_mState.XButton1 == ButtonState.Released);
                case MouseButtons.XButton2:
                    return (_mLast.XButton2 == ButtonState.Pressed) && (_mState.XButton2 == ButtonState.Released);
                default:
                    return false;

            }
        }
        public static bool IsDown(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.Left:
                    return (_mState.LeftButton == ButtonState.Pressed);
                case MouseButtons.Right:
                    return (_mState.RightButton == ButtonState.Pressed);
                case MouseButtons.Middle:
                    return (_mState.MiddleButton == ButtonState.Pressed);
                case MouseButtons.XButton1:
                    return (_mState.XButton1 == ButtonState.Pressed);
                case MouseButtons.XButton2:
                    return (_mState.XButton2 == ButtonState.Pressed);
                default:
                    return false;
            }
        }

        public static bool MouseScrolledUp { get { return (_mLast.ScrollWheelValue < _mState.ScrollWheelValue); } }
        public static bool MouseScrolledDown { get { return (_mLast.ScrollWheelValue > _mState.ScrollWheelValue); } }
        public static int MouseScrollAmnt { get { return (_mState.ScrollWheelValue - _mLast.ScrollWheelValue); } }
        #endregion
        #region GamePad
        public static bool IsConnected(PlayerIndex player)
        {
            return GamePad.GetState(player).IsConnected;
        }
        public static bool IsDown(PlayerIndex player, Buttons button)
        {
            if (GamePad.GetState(player).IsConnected)
                return GamePad.GetState(player).IsButtonDown(button);
            else return false;

            switch (player)
            {
                case PlayerIndex.One:
                    if (_gpAState.IsConnected) { return _gpAState.IsButtonDown(button); }
                    else return false;
                case PlayerIndex.Two:
                    if (_gpBState.IsConnected) { return _gpBState.IsButtonDown(button); }
                    else return false;
                case PlayerIndex.Three:
                    if (_gpCState.IsConnected) { return _gpCState.IsButtonDown(button); }
                    else return false;
                case PlayerIndex.Four:
                    if (_gpDState.IsConnected) { return _gpDState.IsButtonDown(button); }
                    else return false;
                default: return false;
            }
        }
        public static bool IsUp(PlayerIndex player, Buttons button)
        {
            if (GamePad.GetState(player).IsConnected)
                return GamePad.GetState(player).IsButtonUp(button);
            else return false;

            switch (player)
            {
                case PlayerIndex.One:
                    if (_gpAState.IsConnected) { return _gpAState.IsButtonUp(button); }
                    else return false;
                case PlayerIndex.Two:
                    if (_gpBState.IsConnected) { return _gpBState.IsButtonUp(button); }
                    else return false;
                case PlayerIndex.Three:
                    if (_gpCState.IsConnected) { return _gpCState.IsButtonUp(button); }
                    else return false;
                case PlayerIndex.Four:
                    if (_gpDState.IsConnected) { return _gpDState.IsButtonUp(button); }
                    else return false;
                default: return false;
            }
        }
        public static bool IsPressed(PlayerIndex player, Buttons button)
        {
            switch (player)
            {
                case PlayerIndex.One:
                    if (_gpAState.IsConnected)
                    {
                        return _gpAState.IsButtonDown(button)
                            && _gpALast.IsButtonUp(button);
                    }
                    else return false;
                case PlayerIndex.Two:
                    if (_gpBState.IsConnected)
                    {
                        return _gpBState.IsButtonDown(button)
                            && _gpBLast.IsButtonUp(button);
                    }
                    else return false;
                case PlayerIndex.Three:
                    if (_gpCState.IsConnected)
                    {
                        return _gpCState.IsButtonDown(button)
                            && _gpCLast.IsButtonUp(button);
                    }
                    else return false;
                case PlayerIndex.Four:
                    if (_gpDState.IsConnected)
                    {
                        return _gpDState.IsButtonDown(button)
                            && _gpDLast.IsButtonUp(button);
                    }
                    else return false;
                default: return false;
            }
        }
        public static bool IsReleased(PlayerIndex player, Buttons button)
        {
            switch (player)
            {
                case PlayerIndex.One:
                    if (_gpAState.IsConnected)
                    {
                        return _gpAState.IsButtonUp(button)
                            && _gpALast.IsButtonDown(button);
                    }
                    else return false;
                case PlayerIndex.Two:
                    if (_gpBState.IsConnected)
                    {
                        return _gpBState.IsButtonUp(button)
                            && _gpBLast.IsButtonDown(button);
                    }
                    else return false;
                case PlayerIndex.Three:
                    if (_gpCState.IsConnected)
                    {
                        return _gpCState.IsButtonUp(button)
                            && _gpCLast.IsButtonDown(button);
                    }
                    else return false;
                case PlayerIndex.Four:
                    if (_gpDState.IsConnected)
                    {
                        return _gpDState.IsButtonUp(button)
                            && _gpDLast.IsButtonDown(button);
                    }
                    else return false;
                default: return false;
            }
        }
        public static Vector2 LeftStick(PlayerIndex player)
        {
            if (GamePad.GetState(player).IsConnected)
                return GamePad.GetState(player).ThumbSticks.Left;
            else return Vector2.Zero;

            /*switch (player)
            {
                case PlayerIndex.One:
                    if (_gpAState.IsConnected) return _gpAState.ThumbSticks.Left;
                    return Vector2.Zero;
                case PlayerIndex.Two:
                    if (_gpBState.IsConnected) return _gpBState.ThumbSticks.Left;
                    return Vector2.Zero;
                case PlayerIndex.Three:
                    if (_gpCState.IsConnected) return _gpCState.ThumbSticks.Left;
                    return Vector2.Zero;
                case PlayerIndex.Four:
                    if (_gpDState.IsConnected) return _gpDState.ThumbSticks.Left;
                    return Vector2.Zero;
                default:
                    return Vector2.Zero;
            }*/
        }
        public static Vector2 RightStick(PlayerIndex player)
        {
            if (GamePad.GetState(player).IsConnected)
                return GamePad.GetState(player).ThumbSticks.Right;
            else return Vector2.Zero;

            switch (player)
            {
                case PlayerIndex.One:
                    if (_gpAState.IsConnected) return _gpAState.ThumbSticks.Right;
                    return Vector2.Zero;
                case PlayerIndex.Two:
                    if (_gpBState.IsConnected) return _gpBState.ThumbSticks.Right;
                    return Vector2.Zero;
                case PlayerIndex.Three:
                    if (_gpCState.IsConnected) return _gpCState.ThumbSticks.Right;
                    return Vector2.Zero;
                case PlayerIndex.Four:
                    if (_gpDState.IsConnected) return _gpDState.ThumbSticks.Right;
                    return Vector2.Zero;
                default:
                    return Vector2.Zero;
            }
        }
        public static float LeftTrigger(PlayerIndex player)
        {
            if (GamePad.GetState(player).IsConnected)
                return GamePad.GetState(player).Triggers.Left;
            else return 0.0f;

            switch (player)
            {
                case PlayerIndex.One:
                    if (_gpAState.IsConnected) return _gpAState.Triggers.Left;
                    return 0.0f;
                case PlayerIndex.Two:
                    if (_gpBState.IsConnected) return _gpBState.Triggers.Left;
                    return 0.0f;
                case PlayerIndex.Three:
                    if (_gpCState.IsConnected) return _gpCState.Triggers.Left;
                    return 0.0f;
                case PlayerIndex.Four:
                    if (_gpDState.IsConnected) return _gpDState.Triggers.Left;
                    return 0.0f;
                default:
                    return 0.0f;
            }
        }
        public static float RightTrigger(PlayerIndex player)
        {
            if (GamePad.GetState(player).IsConnected)
                return GamePad.GetState(player).Triggers.Right;
            else return 0.0f;

            switch (player)
            {
                case PlayerIndex.One:
                    if (_gpAState.IsConnected) return _gpAState.Triggers.Right;
                    return 0.0f;
                case PlayerIndex.Two:
                    if (_gpBState.IsConnected) return _gpBState.Triggers.Right;
                    return 0.0f;
                case PlayerIndex.Three:
                    if (_gpCState.IsConnected) return _gpCState.Triggers.Right;
                    return 0.0f;
                case PlayerIndex.Four:
                    if (_gpDState.IsConnected) return _gpDState.Triggers.Right;
                    return 0.0f;
                default:
                    return 0.0f;
            }
        }
        public static void SetVibration(PlayerIndex player,
            float lowMotor, float highMotor)
        {
            GamePad.SetVibration(player, lowMotor, highMotor);
        }
        #endregion
        #region Flags
        public static bool IsMouseOrGamepadHandled { get { return isMouseOrGamepadHandled; } set { isMouseOrGamepadHandled = value; } }
        #endregion
        #region Actions
        public static Dictionary<string, InputAction> Actions(PlayerIndex player)
        {
            switch (player)
            {
                case PlayerIndex.One:
                    return ActionsA;
                case PlayerIndex.Two:
                    return ActionsB;
                case PlayerIndex.Three:
                    return ActionsC;
                case PlayerIndex.Four:
                    return ActionsD;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Adds an empty InputAction to all player.
        /// </summary>
        /// <param name="Name">The Name of the Action</param>
        public static void ActionAddAll(string Name)
        {
            ActionsA.Add(Name, new InputCommand(PlayerIndex.One, null, null, null));
            ActionsB.Add(Name, new InputCommand(PlayerIndex.Two, null, null, null));
            ActionsC.Add(Name, new InputCommand(PlayerIndex.Three, null, null, null));
            ActionsD.Add(Name, new InputCommand(PlayerIndex.Four, null, null, null));
        }

        /// <summary>
        /// Adds an InputAction to all players.
        /// </summary>
        /// <param name="Name">The Name of the Action</param>
        /// <param name="key">The Key it contains</param>
        public static void ActionAddAll(string Name, Keys key)
        {
            ActionsA.Add(Name, new InputCommand(PlayerIndex.One, key, null, null));
            ActionsB.Add(Name, new InputCommand(PlayerIndex.Two, key, null, null));
            ActionsC.Add(Name, new InputCommand(PlayerIndex.Three, key, null, null));
            ActionsD.Add(Name, new InputCommand(PlayerIndex.Four, key, null, null));
        }

        /// <summary>
        /// Adds a new InputAction to all players.
        /// </summary>
        /// <param name="Name">The Name of the Action</param>
        /// <param name="button">The MouseButton it contains</param>
        public static void ActionAddAll(string Name, MouseButtons button)
        {
            ActionsA.Add(Name, new InputCommand(PlayerIndex.One, null, button, null));
            ActionsB.Add(Name, new InputCommand(PlayerIndex.Two, null, button, null));
            ActionsC.Add(Name, new InputCommand(PlayerIndex.Three, null, button, null));
            ActionsD.Add(Name, new InputCommand(PlayerIndex.Four, null, button, null));
        }

        /// <summary>
        /// Adds a new InputAction to all players.
        /// </summary>
        /// <param name="Name">The Name of the Action</param>
        /// <param name="button">The Button(GamePad) it contains</param>
        public static void ActionAddAll(string Name, Buttons button)
        {
            ActionsA.Add(Name, new InputCommand(PlayerIndex.One, null, null, button));
            ActionsB.Add(Name, new InputCommand(PlayerIndex.Two, null, null, button));
            ActionsC.Add(Name, new InputCommand(PlayerIndex.Three, null, null, button));
            ActionsD.Add(Name, new InputCommand(PlayerIndex.Four, null, null, button));
        }

        /// <summary>
        /// Adds an InputAction to specified player.
        /// </summary>
        /// <param name="Name">The Name of the Action</param>
        /// <param name="player">The Player who owns it</param>
        /// <param name="key">The Key it contains</param>
        public static void ActionAdd(string Name, PlayerIndex player, Keys key)
        {
            switch (player)
            {
                case PlayerIndex.One:
                    ActionsA.Add(Name, new InputCommand(player, key, null, null));
                    break;
                case PlayerIndex.Two:
                    ActionsB.Add(Name, new InputCommand(player, key, null, null));
                    break;
                case PlayerIndex.Three:
                    ActionsC.Add(Name, new InputCommand(player, key, null, null));
                    break;
                case PlayerIndex.Four:
                    ActionsD.Add(Name, new InputCommand(player, key, null, null));
                    break;
            }
        }

        /// <summary>
        /// Adds a new InputAction to specified player.
        /// </summary>
        /// <param name="Name">The Name of the Action</param>
        /// <param name="player">The Player who owns it</param>
        /// <param name="button">The MouseButton it contains</param>
        public static void ActionAdd(string Name, PlayerIndex player, MouseButtons button)
        {
            switch (player)
            {
                case PlayerIndex.One:
                    ActionsA.Add(Name, new InputCommand(player, null, button, null));
                    break;
                case PlayerIndex.Two:
                    ActionsB.Add(Name, new InputCommand(player, null, button, null));
                    break;
                case PlayerIndex.Three:
                    ActionsC.Add(Name, new InputCommand(player, null, button, null));
                    break;
                case PlayerIndex.Four:
                    ActionsD.Add(Name, new InputCommand(player, null, button, null));
                    break;
            }
        }

        /// <summary>
        /// Adds a new InputAction to specified player.
        /// </summary>
        /// <param name="Name">The Name of the Action</param>
        /// <param name="player">The Player who owns it</param>
        /// <param name="button">The Button(Gamepad) it contains</param>
        public static void ActionAdd(string Name, PlayerIndex player, Buttons button)
        {
            switch (player)
            {
                case PlayerIndex.One:
                    ActionsA.Add(Name, new InputCommand(player, null, null, button));
                    break;
                case PlayerIndex.Two:
                    ActionsB.Add(Name, new InputCommand(player, null, null, button));
                    break;
                case PlayerIndex.Three:
                    ActionsC.Add(Name, new InputCommand(player, null, null, button));
                    break;
                case PlayerIndex.Four:
                    ActionsD.Add(Name, new InputCommand(player, null, null, button));
                    break;
            }
        }
        #endregion
        #endregion
    }
}
