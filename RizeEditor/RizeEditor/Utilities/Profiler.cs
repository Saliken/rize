﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace RizeEditor
{
    static class Profiler
    {
        private static Dictionary<string, Stopwatch> timeSpans;

        static Profiler()
        {
            timeSpans = new Dictionary<string, Stopwatch>();
        }

        public static void Start(string Name)
        {
            if (timeSpans.ContainsKey(Name)) timeSpans[Name].Restart();
            else timeSpans.Add(Name, Stopwatch.StartNew());
        }

        public static TimeSpan EndR(string Name)
        {
            if (timeSpans.ContainsKey(Name))
            {
                timeSpans[Name].Stop();
                return timeSpans[Name].Elapsed;
            }
            else return TimeSpan.MinValue;
        }

        public static void End(string Name)
        {
            if(timeSpans.ContainsKey(Name))
            {
            timeSpans[Name].Stop();
            Console.WriteLine(Name + ": " + timeSpans[Name].ElapsedMilliseconds + " Milliseconds" + "               ");
            }
        }

        public static TimeSpan Time(Action action)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            action();
            stopwatch.Stop();
            return stopwatch.Elapsed;
        }
       
    }
}
